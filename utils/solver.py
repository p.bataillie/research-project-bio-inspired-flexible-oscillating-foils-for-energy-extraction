from models import model_greenberg, model_theodorsen, model_garrick, model_LDVM
from utils import time_manager, results, input_data
import numpy as np


class Solver:
    def __init__(self, initial_data=None, filepath=None, case_set=None):
        if initial_data is None and filepath is not None and case_set is not None:
            self.initial_data = input_data.load_data(filepath, case_set)
        elif initial_data is not None and filepath is None and case_set is None:
            self.initial_data = initial_data
        else:
            assert False, 'Solver Inputs Error.'

        self.model = []

        for i in range(len(self.initial_data)):
            if self.initial_data[i].case.model == "Garrick":
                self.model.append(model_garrick.GarrickModel(self.initial_data[i]))
            elif self.initial_data[i].case.model == "Theodorsen":
                self.model.append(model_theodorsen.TheodorsenModel(self.initial_data[i]))
            elif self.initial_data[i].case.model == "Greenberg":
                self.model.append(model_greenberg.GreenbergModel(self.initial_data[i]))
            elif self.initial_data[i].case.model == "LDVM":
                self.model.append(model_LDVM.LDVMModel(self.initial_data[i]))

    def solve(self, func=None):
        for i in range(len(self.initial_data)):
            for time_index in range(np.size(self.model[i].time_manager.time)):
                self.model[i].movements_manager(time_index, func)
                self.model[i].compute_forces(time_index)

            self.model[i].compute_forces_batch()
            self.model[i].postprocessing()

    def output(self, output_type='results'):
        if output_type == 'results':
            results_obj = []

            for i in range(len(self.initial_data)):
                results_obj.append(results.Results(self.model[i]))
                results_obj[i].compute_power()
                results_obj[i].compute_energy()
                results_obj[i].compute_efficiency()

            if len(results_obj) == 1:
                return results_obj[0]
            else:
                return results_obj

        elif output_type == 'model':
            if len(self.model) == 1:
                return self.model[0]
            else:
                return self.model

        else:
            return None
