import numpy as np
from matplotlib import pyplot as plt
from models import model_greenberg, model_theodorsen, model_garrick, model_LDVM
from utils import csv_utils


class Results:
    def __init__(self, model):
        self.model = model
        self.name = model.initial_data.case.name

        self.time = model.time
        self.time_size = np.size(self.time)

        self.period = model.period
        if self.period is not None:
            self.time_over_period = self.time / self.period

        self.time_step = model.time_step

        self.rho = model.rho
        self.temperature = model.temperature
        self.dynamic_viscosity = model.dynamic_viscosity

        self.frequency = model.initial_data.case.movement.frequency
        self.angular_frequency = model.initial_data.case.movement.angular_frequency
        self.reduced_frequency = model.initial_data.case.movement.reduced_frequency

        self.critical_lesp = model.initial_data.case.airfoil.critical_lesp

        self.pitching_phase = model.phi_0
        self.heaving_phase = model.phi_2
        self.aileron_pitching_phase = model.phi_1

        self.aileron_starting_position = model.initial_data.case.airfoil.aileron_starting_position
        self.rotation_axis_position = model.initial_data.case.airfoil.rotation_axis_position
        self.semi_chord = model.initial_data.case.airfoil.semi_chord
        self.chord = model.initial_data.case.airfoil.chord

        self.rotation_axis_in_percentage_chord = model.initial_data.case.airfoil.rotation_axis_in_percentage_chord

        self.u_inf = model.u_inf
        self.dynamic_pressure = (1 / 2) * self.rho * (self.u_inf ** 2)

        self.time_dimensionless = (self.time * self.u_inf) / self.chord

        self.reynolds_number = model.initial_data.case.flow.reynolds_number

        if model.initial_data.case.movement.frequency is not None:
            self.f_star = (model.initial_data.case.movement.frequency * self.semi_chord) / self.u_inf
        else:
            self.f_star = None

        self.heaving_amplitude = model.initial_data.case.movement.heaving_amplitude
        self.mean_heaving_amplitude = model.initial_data.case.movement.mean_heaving_amplitude

        self.pitching_amplitude = model.initial_data.case.movement.pitching_amplitude
        self.mean_pitching_amplitude = model.initial_data.case.movement.mean_pitching_amplitude

        self.aileron_pitching_amplitude = model.initial_data.case.movement.tail_pitching_amplitude
        self.mean_aileron_pitching_amplitude = model.initial_data.case.movement.mean_tail_pitching_amplitude

        self.lift_force_total = model.lift_force_total * (self.chord / model.c)
        self.lift_force_airfoil = model.lift_force_airfoil * (self.chord / model.c)
        self.lift_force_aileron = model.lift_force_aileron * (self.chord / model.c)
        self.lift_coefficient = model.lift_coefficient

        self.drag_force_total = model.drag_force_total * (self.chord / model.c)
        self.drag_force_airfoil = model.drag_force_airfoil * (self.chord / model.c)
        self.drag_force_aileron = model.drag_force_aileron * (self.chord / model.c)
        self.drag_coefficient = model.drag_coefficient

        self.propulsive_force_total = model.propulsive_force_total * (self.chord / model.c)
        self.propulsive_force_airfoil = model.propulsive_force_airfoil * (self.chord / model.c)
        self.propulsive_force_aileron = model.propulsive_force_aileron * (self.chord / model.c)
        self.propulsive_coefficient = model.propulsive_coefficient

        self.moment_airfoil = model.moment_airfoil * ((self.chord / model.c) ** 2)
        self.moment_aileron = model.moment_aileron * ((self.chord / model.c) ** 2)
        self.moment_airfoil_coefficient = model.moment_airfoil_coefficient
        self.moment_aileron_coefficient = model.moment_aileron_coefficient

        self.heaving_power = np.zeros(self.time_size)
        self.heaving_power_control = np.zeros(self.time_size)
        self.pitching_power = np.zeros(self.time_size)
        self.pitching_power_control = np.zeros(self.time_size)
        self.drag_power = np.zeros(self.time_size)

        self.power_available_oncoming_flow = None

        self.power_input = np.zeros(self.time_size)
        self.power_output = np.zeros(self.time_size)

        self.efficiency = None
        self.froude_efficiency = None

        self.heaving_energy = None
        self.heaving_energy_control = None
        self.pitching_energy = None
        self.pitching_energy_control = None
        self.drag_energy = None

        self.feathering_parameter = None

        if self.heaving_amplitude is not None and self.angular_frequency is not None:
            if self.heaving_amplitude != 0.0 and self.angular_frequency != 0.0:
                self.feathering_parameter = self.pitching_amplitude / np.arctan((self.heaving_amplitude * self.angular_frequency) / self.u_inf)
        else:
            self.feathering_parameter = None

        if (isinstance(model, model_theodorsen.TheodorsenModel)
                or isinstance(model, model_garrick.GarrickModel)
                or isinstance(model, model_greenberg.GreenbergModel)):

            self.u = self.model.u.imag

            self.h = self.model.h.imag
            self.theta = self.model.theta.imag
            self.delta = self.model.delta.imag

            self.h_dot = self.model.h_dot.imag
            self.theta_dot = self.model.theta_dot.imag
            self.delta_dot = self.model.delta_dot.imag

            self.h_dot_2 = self.model.h_dot_2.imag
            self.theta_dot_2 = self.model.theta_dot_2.imag
            self.delta_dot_2 = self.model.delta_dot_2.imag

            self.alpha_eff = self.model.alpha_eff
        else:
            self.theta = model.theta
            self.theta_dot = np.insert(np.diff(model.theta) / model.time_manager.dt, 0, np.nan)
            self.theta_dot_2 = np.insert(np.diff(np.diff(model.theta) / model.time_manager.dt) / model.time_manager.dt, 0, np.nan)
            self.theta_dot_2 = np.insert(self.theta_dot_2, 0, np.nan)
            self.h = model.h
            self.h_dot = np.insert(np.diff(model.h) / model.time_manager.dt, 0, np.nan)
            self.h_dot_2 = np.insert(np.diff(np.diff(model.h) / model.time_manager.dt) / model.time_manager.dt, 0, np.nan)
            self.h_dot_2 = np.insert(self.h_dot_2, 0, np.nan)
            self.alpha_eff = self.theta + np.arctan(self.h_dot / self.u_inf)
            self.u = model.u
            self.delta = self.model.delta
            self.delta_dot = self.model.delta_dot

        self.time_over_period_exp = None
        self.lift_coefficient_exp = None
        self.drag_coefficient_exp = None
        self.moment_coefficient_exp = None

        if model.initial_data.case.experimental_data.lift_coefficient is not None:
            self.load_experimental_data('lift_coefficient', model.initial_data.case.experimental_data.lift_coefficient)
        if model.initial_data.case.experimental_data.drag_coefficient is not None:
            self.load_experimental_data('drag_coefficient', model.initial_data.case.experimental_data.drag_coefficient)
        if model.initial_data.case.experimental_data.moment_coefficient is not None:
            self.load_experimental_data('moment_coefficient', model.initial_data.case.experimental_data.moment_coefficient)

    def compute_power(self):
        for time_index in range(np.size(self.time)):
            self.heaving_power[time_index] = self.lift_force_total[time_index] * self.h_dot[time_index]
            self.heaving_power_control[time_index] = - self.heaving_power[time_index]
            self.pitching_power[time_index] = self.moment_airfoil[time_index] * self.theta_dot[time_index]
            self.pitching_power_control[time_index] = - self.pitching_power[time_index]
            self.drag_power[time_index] = self.drag_force_total[time_index] * self.u[time_index]
            # if self.feathering_parameter >= 1.0:
            self.power_input[time_index] = self.drag_force_total[time_index] * self.u[time_index]
            self.power_output[time_index] = (self.lift_force_total[time_index] * self.h_dot[time_index]
                                             + self.moment_airfoil[time_index] * self.theta_dot[time_index]
                                             + self.moment_aileron[time_index] * self.delta_dot[time_index])
            # else:
            #     self.power_input[time_index] = - (self.lift_force_total[time_index] * self.h_dot[time_index]
            #                                       + self.moment_airfoil[time_index] * self.theta_dot[time_index]
            #                                       + self.moment_aileron[time_index] * self.delta_dot[time_index])
            #     self.power_output[time_index] = self.propulsive_force_total[time_index] * self.u[time_index]

    def compute_energy(self):
        self.heaving_energy = np.trapz(self.heaving_power, x=self.time)
        self.pitching_energy = np.trapz(self.pitching_power, x=self.time)
        self.heaving_energy_control = np.trapz(self.heaving_power_control, x=self.time)
        self.pitching_energy_control = np.trapz(self.pitching_power_control, x=self.time)

    def compute_efficiency(self):
        vertical_extent_airfoil_motion = 2.0 * np.abs(self.h).max() + self.semi_chord * np.tan(np.abs(self.theta[np.abs(self.h_dot).argmin()]))
        self.power_available_oncoming_flow = self.dynamic_pressure * self.u_inf * vertical_extent_airfoil_motion
        self.efficiency = np.mean(self.power_output) / self.power_available_oncoming_flow
        self.froude_efficiency = np.mean(self.power_output) / np.mean(self.power_input)

    def show_movement(self, show_plot=False, save_figure=None, debug=False):
        if debug:
            fig, axs = plt.subplots(3, 3, figsize=(12, 8))
            plt.subplots_adjust(left=0.1, bottom=0.25, right=0.9, top=0.75, wspace=0.4, hspace=0.3)

            self.plot_2d(axs=axs[0][0],
                         data=[self.time_over_period, self.h],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', 'h'])

            self.plot_2d(axs=axs[0][1],
                         data=[self.time_over_period, np.degrees(self.theta)],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$\\theta$'])

            self.plot_2d(axs=axs[0][2],
                         data=[self.time_over_period, np.degrees(self.delta)],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$\\delta$'])

            self.plot_2d(axs=axs[1][0],
                         data=[self.time_over_period, self.h_dot],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$h^{\'}$'])

            self.plot_2d(axs=axs[1][1],
                         data=[self.time_over_period, np.degrees(self.theta_dot)],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$\\theta^{\'}$'])

            self.plot_2d(axs=axs[1][2],
                         data=[self.time_over_period, np.degrees(self.delta_dot)],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$\\delta^{\'}$'])

            self.plot_2d(axs=axs[2][0],
                         data=[self.time_over_period, self.h_dot_2],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$h^{\'\'}$'])

            self.plot_2d(axs=axs[2][1],
                         data=[self.time_over_period, np.degrees(self.theta_dot_2)],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$\\theta^{\'\'}$'])

            self.plot_2d(axs=axs[2][2],
                         data=[self.time_over_period, np.degrees(self.delta_dot_2)],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$\\delta^{\'\'}$'])
        else:
            fig, axs = plt.subplots(1, 4, figsize=(13, 4))
            plt.subplots_adjust(left=0.1, bottom=0.25, right=0.9, top=0.75, wspace=0.4, hspace=0.3)

            self.plot_2d(axs=axs[0],
                         data=[self.time_over_period, self.h],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$h$'])

            self.plot_2d(axs=axs[1],
                         data=[self.time_over_period, np.degrees(self.theta)],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$\\theta$'])

            self.plot_2d(axs=axs[2],
                         data=[self.time_over_period, np.degrees(self.delta)],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$\\delta$'])

            self.plot_2d(axs=axs[3],
                         data=[self.time_over_period, np.degrees(self.alpha_eff)],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$\\alpha_{eff}$'])

        if save_figure is not None:
            fig.savefig('figures/' + save_figure + '.svg')
        if show_plot is True:
            plt.show()

    def show_lift_coefficient(self, show_plot=False, save_figure=None):
        fig, axs = plt.subplots(1, 1)
        if isinstance(self.model, model_garrick.GarrickModel):
            self.plot_2d(axs=axs,
                         data=[self.time_over_period, self.lift_coefficient],
                         experimental_data=[self.time_over_period_exp, self.lift_coefficient_exp],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                         curve_labels=['Garrick', 'Experimental Data'])

        elif isinstance(self.model, model_theodorsen.TheodorsenModel):
            self.plot_2d(axs=axs,
                         data=[self.time_over_period, self.lift_coefficient],
                         experimental_data=[self.time_over_period_exp, self.lift_coefficient_exp],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                         curve_labels=['Theodorsen', 'Experimental Data'])

        elif isinstance(self.model, model_greenberg.GreenbergModel):
            self.plot_2d(axs=axs,
                         data=[self.time_over_period, self.lift_coefficient],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                         curve_labels=['Greenberg'])

        if save_figure is not None:
            fig.savefig('figures/' + save_figure + '.svg')
        if show_plot is True:
            fig.show()

    def show_drag_coefficient(self, show_plot=False, save_figure=None):
        fig, axs = plt.subplots(1, 1)
        if isinstance(self.model, model_garrick.GarrickModel):
            self.plot_2d(axs=axs,
                         data=[self.time_over_period, self.drag_coefficient],
                         experimental_data=[self.time_over_period_exp, self.drag_coefficient_exp],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                         curve_labels=['Garrick', 'Experimental Data'])

        elif isinstance(self.model, model_theodorsen.TheodorsenModel):
            self.plot_2d(axs=axs,
                         data=[self.time_over_period, self.drag_coefficient],
                         experimental_data=[self.time_over_period_exp, self.drag_coefficient_exp],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                         curve_labels=['Theodorsen', 'Experimental Data'])

        elif isinstance(self.model, model_greenberg.GreenbergModel):
            self.plot_2d(axs=axs,
                         data=[self.time_over_period, self.drag_coefficient],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                         curve_labels=['Greenberg'])

        if save_figure is not None:
            fig.savefig('figures/' + save_figure + '.svg')
        if show_plot is True:
            fig.show()

    def load_experimental_data(self, quantity, filepath):
        experimental_values = csv_utils.read_csv(path=filepath)
        if quantity == 'lift_coefficient':
            self.time_over_period_exp = experimental_values[:, 0]
            self.lift_coefficient_exp = experimental_values[:, 1]
        if quantity == 'drag_coefficient':
            self.drag_coefficient_exp = np.interp(self.time_over_period_exp,
                                                  experimental_values[:, 0],
                                                  experimental_values[:, 1])
        if quantity == 'moment_coefficient':
            self.moment_coefficient_exp = np.interp(self.time_over_period_exp,
                                                    experimental_values[:, 0],
                                                    experimental_values[:, 1])

    @staticmethod
    def plot_general(axs=None, data=None, axis_labels=None, x_range=None, y_range=None, legend_size=None):
        for i in range(len(data)):
            if data[i][2] == 'num':
                axs.plot(data[i][0], data[i][1], label=data[i][3], color=data[i][4])
            elif data[i][2] == 'exp':
                axs.scatter(data[i][0], data[i][1], label=data[i][3], color=data[i][4], s=8)

        if x_range is not None:
            axs.set_xlim(left=x_range[0], right=x_range[1])
        if y_range is not None:
            axs.set_ylim(bottom=y_range[0], top=y_range[1])

        axs.grid()

        if legend_size is not None:
            axs.legend(fontsize=legend_size)
        else:
            axs.legend()

        if axis_labels is not None:
            axs.set_xlabel(axis_labels[0])
            axs.set_ylabel(axis_labels[1])

    @staticmethod
    def plot_2d(axs=None, data=None, experimental_data=None, axis_labels=None, curve_labels=None, x_range=None,
                y_range=None):
        if curve_labels is None:
            axs.plot(data[0], data[1], color='black')
            if experimental_data is not None:
                axs.scatter(experimental_data[0], experimental_data[1], color='black', s=8)
        else:
            axs.plot(data[0], data[1], label=curve_labels[0], color='black')
            if experimental_data is not None:
                axs.scatter(experimental_data[0], experimental_data[1], label=curve_labels[1], color='black', s=8)
        axs.grid()
        if x_range is not None:
            axs.set_xlim(left=x_range[0], right=x_range[1])
        if y_range is not None:
            axs.set_ylim(bottom=y_range[0], top=y_range[1])
        if curve_labels is not None:
            axs.legend()
        if axis_labels is not None:
            axs.set_xlabel(axis_labels[0])
            axs.set_ylabel(axis_labels[1])

    def show_lift_drag_general(self,
                               data,
                               axis_labels,
                               y_range=None,
                               show_plot=False,
                               save_figure=False,
                               figures_path=None,
                               legend_size=None):

        fig, axs = plt.subplots(1, len(data), figsize=(12, 5))
        plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9, wspace=0.4, hspace=0.3)

        for i in range(len(data)):
            if y_range is not None:
                self.plot_general(axs=axs[i],
                                  data=data[i],
                                  axis_labels=axis_labels[i],
                                  y_range=y_range[i],
                                  legend_size=legend_size)
            else:
                self.plot_general(axs=axs[i],
                                  data=data[i],
                                  axis_labels=axis_labels[i],
                                  legend_size=legend_size)

        if self.f_star is not None:
            plt.suptitle('$U_{\\infty} = $' + str(round(self.u_inf, 2)) + ' $m.s^{-1}$ | $f^{*}$ = ' + str(round(self.f_star, 4)) + ' | $\\theta_{0}$ = ' + str(round(np.degrees(self.pitching_amplitude), 2)) + '$^{\\circ}$ | $h_{0}$/c = ' + str(round(self.heaving_amplitude/self.chord, 2)) + ' | $x_{p}$/c = ' + str(round(self.rotation_axis_in_percentage_chord, 2)), fontsize=10.5)

        if save_figure is True:
            fig.savefig(figures_path + self.name + '_lift_drag_coefficients.svg')
        if show_plot is True:
            plt.show()

    def show_lift_drag(self, show_plot=False, save_figure=False, figures_path=None, lift_coefficient_range=None,
                       drag_coefficient_range=None):

        fig, axs = plt.subplots(1, 2, figsize=(12, 5))
        plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9, wspace=0.4, hspace=0.3)

        if isinstance(self.model, model_garrick.GarrickModel):
            if self.lift_coefficient_exp is not None and self.drag_coefficient_exp is not None:
                self.plot_2d(axs=axs[0],
                             data=[self.time_over_period, self.lift_coefficient],
                             experimental_data=[self.time_over_period_exp, self.lift_coefficient_exp],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                             curve_labels=['Theodorsen', 'Experimental Data'],
                             y_range=lift_coefficient_range)
                self.plot_2d(axs=axs[1],
                             data=[self.time_over_period, self.drag_coefficient],
                             experimental_data=[self.time_over_period_exp, self.drag_coefficient_exp],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                             curve_labels=['Garrick', 'Experimental Data'],
                             y_range=drag_coefficient_range)
            else:
                self.plot_2d(axs=axs[0],
                             data=[self.time_over_period, self.lift_coefficient],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                             curve_labels=['Theodorsen', 'Experimental Data'],
                             y_range=lift_coefficient_range)
                self.plot_2d(axs=axs[1],
                             data=[self.time_over_period, self.drag_coefficient],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                             curve_labels=['Garrick', 'Experimental Data'],
                             y_range=drag_coefficient_range)

        elif isinstance(self.model, model_theodorsen.TheodorsenModel):
            if self.lift_coefficient_exp is not None and self.drag_coefficient_exp is not None:
                self.plot_2d(axs=axs[0],
                             data=[self.time_over_period, self.lift_coefficient],
                             experimental_data=[self.time_over_period_exp, self.lift_coefficient_exp],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                             curve_labels=['Theodorsen', 'Experimental Data'],
                             y_range=lift_coefficient_range)
                self.plot_2d(axs=axs[1],
                             data=[self.time_over_period, self.drag_coefficient],
                             experimental_data=[self.time_over_period_exp, self.drag_coefficient_exp],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                             curve_labels=['Theodorsen', 'Experimental Data'],
                             y_range=drag_coefficient_range)
            else:
                self.plot_2d(axs=axs[0],
                             data=[self.time_over_period, self.lift_coefficient],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                             curve_labels=['Theodorsen', 'Experimental Data'],
                             y_range=lift_coefficient_range)
                self.plot_2d(axs=axs[1],
                             data=[self.time_over_period, self.drag_coefficient],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                             curve_labels=['Theodorsen', 'Experimental Data'],
                             y_range=drag_coefficient_range)

        elif isinstance(self.model, model_greenberg.GreenbergModel):
            if self.lift_coefficient_exp is not None and self.drag_coefficient_exp is not None:
                self.plot_2d(axs=axs[0],
                             data=[self.time_over_period, self.lift_coefficient],
                             experimental_data=[self.time_over_period_exp, self.lift_coefficient_exp],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                             curve_labels=['Greenberg', 'Experimental Data'])
                self.plot_2d(axs=axs[1],
                             data=[self.time_over_period, self.drag_coefficient],
                             experimental_data=[self.time_over_period_exp, self.drag_coefficient_exp],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                             curve_labels=['Greenberg', 'Experimental Data'])
            else:
                self.plot_2d(axs=axs[0],
                             data=[self.time_over_period, self.lift_coefficient],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                             curve_labels=['Greenberg'])
                self.plot_2d(axs=axs[1],
                             data=[self.time_over_period, self.drag_coefficient],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                             curve_labels=['Greenberg'])

        elif isinstance(self.model, model_LDVM.LDVMModel):
            if self.lift_coefficient_exp is not None and self.drag_coefficient_exp is not None:
                self.plot_2d(axs=axs[0],
                             data=[self.time_over_period, self.lift_coefficient],
                             experimental_data=[self.time_over_period_exp, self.lift_coefficient_exp],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                             curve_labels=['LDVM', 'Experimental Data'])
                self.plot_2d(axs=axs[1],
                             data=[self.time_over_period, self.drag_coefficient],
                             experimental_data=[self.time_over_period_exp, self.drag_coefficient_exp],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                             curve_labels=['LDVM', 'Experimental Data'])
            else:
                self.plot_2d(axs=axs[0],
                             data=[self.time_over_period, self.lift_coefficient],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                             curve_labels=['LDVM'])
                self.plot_2d(axs=axs[1],
                             data=[self.time_over_period, self.drag_coefficient],
                             axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                             curve_labels=['LDVM'])

        plt.suptitle('$U_{\\infty} = $' + str(round(self.u_inf, 2)) + ' $m.s^{-1}$ | $f^{*}$ = ' + str(round(self.f_star, 4)) + ' | $\\theta_{0}$ = ' + str(round(np.degrees(self.pitching_amplitude), 2)) + '$^{\\circ}$ | $h_{0}$/c = ' + str(round(self.heaving_amplitude/self.chord, 2)) + ' | $x_{p}$/c = ' + str(round(self.rotation_axis_in_percentage_chord, 2)), fontsize=10.5)

        if save_figure is True:
            fig.savefig(figures_path + self.name + '_lift_drag_coefficients.svg')
        if show_plot is True:
            plt.show()

    def show_moment_theta_general(self,
                               data,
                               axis_labels,
                               y_range=None,
                               show_plot=False,
                               save_figure=False,
                               figures_path=None):

        fig, axs = plt.subplots(1, 1)
        # plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9, wspace=0.4, hspace=0.3)

        self.plot_general(axs=axs,
                          data=data[0],
                          axis_labels=axis_labels[0],
                          y_range=y_range[0])

        axs.set_title('$U_{\\infty} = $' + str(round(self.u_inf, 2)) + ' $m.s^{-1}$ | $f^{*}$ = ' + str(
            round(self.f_star, 4)) + ' | $\\theta_{0}$ = ' + str(
            round(np.degrees(self.pitching_amplitude), 2)) + '$^{\\circ}$ | $h_{0}$/c = ' + str(
            round(self.heaving_amplitude / self.chord, 2)) + ' | $x_{p}$/c = ' + str(
            round(self.rotation_axis_in_percentage_chord, 2)), fontsize=10)

        if save_figure is True:
            fig.savefig(figures_path + self.name + '_moment_coefficient.svg')
        if show_plot is True:
            plt.show()

    def show_moment_theta(self, show_plot=False, save_figure=False, figures_path=None, moment_coefficient_range=None):

        fig, axs = plt.subplots(1, 1)
        # plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9, wspace=0.4, hspace=0.3)
        if self.moment_coefficient_exp is not None:
            self.plot_2d(axs=axs,
                         data=[self.time_over_period, self.moment_airfoil_coefficient],
                         experimental_data=[self.time_over_period_exp, self.moment_coefficient_exp],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{m_{\\theta}}$'],
                         curve_labels=['Theodorsen', 'Experimental Data'],
                         y_range=moment_coefficient_range)
        else:
            self.plot_2d(axs=axs,
                         data=[self.time_over_period, self.moment_airfoil_coefficient],
                         axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{m_{\\theta}}$'],
                         curve_labels=['Theodorsen'],
                         y_range=moment_coefficient_range)

        axs.set_title('$U_{\\infty} = $' + str(round(self.u_inf, 2)) + ' $m.s^{-1}$ | $f^{*}$ = ' + str(round(self.f_star, 4)) + ' | $\\theta_{0}$ = ' + str(round(np.degrees(self.pitching_amplitude), 2)) + '$^{\\circ}$ | $h_{0}$/c = ' + str(round(self.heaving_amplitude/self.chord, 2)) + ' | $x_{p}$/c = ' + str(round(self.rotation_axis_in_percentage_chord, 2)), fontsize=10)

        if save_figure is True:
            fig.savefig(figures_path + self.name + '_moment_coefficient.svg')
        if show_plot is True:
            plt.show()

    def show_moments(self, show_plot=False, save_figure=False, figures_path=None):

        fig, axs = plt.subplots(1, 2, figsize=(12, 5))
        plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9, wspace=0.4, hspace=0.3)

        self.plot_2d(axs=axs[0],
                     data=[self.time_over_period, self.moment_airfoil],
                     axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$M_{\\theta}$ ($N.m$)'],
                     curve_labels=['Garrick'])
        self.plot_2d(axs=axs[1],
                     data=[self.time_over_period, self.moment_aileron],
                     axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$M_{\\delta}$ ($N.m$)'],
                     curve_labels=['Garrick'])

        if save_figure is True:
            fig.savefig(figures_path + self.name + '.svg')
        if show_plot is True:
            plt.show()

    def show_power(self, show_plot=False, save_figure=False, figures_path=None):

        fig, axs = plt.subplots(1, 2, figsize=(12, 5))
        plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9, wspace=0.4, hspace=0.3)

        self.plot_2d(axs=axs[0],
                     data=[self.time_over_period, self.heaving_power],
                     axis_labels=['Dimensionless Time $\\frac{t}{T}$', 'Required Heaving Power (W)'],
                     curve_labels=['Garrick'])
        self.plot_2d(axs=axs[1],
                     data=[self.time_over_period, self.pitching_power],
                     axis_labels=['Dimensionless Time $\\frac{t}{T}$', 'Required Pitching Power (W)'],
                     curve_labels=['Garrick'])

        if save_figure is True:
            fig.savefig(figures_path + self.name + '.svg')
        if show_plot is True:
            plt.show()

    def show_infos(self):
        print("Feathering Parameter: " + str(self.feathering_parameter))
        if self.feathering_parameter > 1.0:
            print('POWER EXTRACTION MODE')
        else:
            print('PROPULSION MODE')
        print('----------------------------------------------')
        print("Freestream Velocity: " + str(self.u_inf) + " m/s.")
        print('Reynolds Number: ' + str(self.reynolds_number))
        print('----------------------------------------------')
        print("Frequency: " + str(self.frequency) + ' Hz.')
        print("f_star: " + str(self.f_star))
        print("Heaving Amplitude: " + str(self.heaving_amplitude) + " m.")
        print("Heaving Phase: " + str(np.degrees(self.heaving_phase)) + "°.")
        print("Pitching Amplitude: " + str(np.degrees(self.pitching_amplitude)) + "°.")
        print("Pitching Phase: " + str(np.degrees(self.pitching_phase)) + "°.")
        print("Aileron Pitching Amplitude: " + str(np.degrees(self.aileron_pitching_amplitude)) + "°.")
        print("Aileron Pitching Phase: " + str(np.degrees(self.aileron_pitching_phase)) + "°.")
        print('----------------------------------------------')
        print("Pitching Energy: " + str(self.pitching_energy) + " J.")
        print("Heaving Energy: " + str(self.heaving_energy) + " J.")
        print('----------------------------------------------')
        print("Aerodynamic Efficiency: " + str(self.efficiency))

    @staticmethod
    def rmsd(array_1, array_2):
        diff = np.sqrt(np.mean(np.power(np.array(array_1 - array_2), 2)))
        return diff
