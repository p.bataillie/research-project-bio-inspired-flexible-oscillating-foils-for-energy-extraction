import numpy as np


def read_csv(path=None, delimiter=",", dtype=float, skip=1):
    arr = np.loadtxt(path, delimiter=delimiter, dtype=dtype, skiprows=skip)
    return arr


def profile_drag_xfoil(alpha):
    alpha_deg = np.round(np.degrees(alpha), 1)
    xfoil_values = read_csv(path='experimental_data/xfoil_data/drag_coefficients.csv', delimiter=";", dtype=float, skip=1)
    alpha_values = xfoil_values[:, 0]
    profile_drag_values = xfoil_values[:, 3]
    profile_drag = profile_drag_values[(np.abs(alpha_values - alpha_deg)).argmin()]
    return profile_drag
