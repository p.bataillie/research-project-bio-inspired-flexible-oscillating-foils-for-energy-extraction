import numpy as np


class TimeManager:
    def __init__(self, data):
        # Period and time step
        self.nb_time_steps = data.solver.nb_time_steps
        self.duration = data.solver.duration

        if self.duration is None:
            if data.movement.period != 0.0:
                self.period = data.movement.period
            elif data.flow.surge_period != 0.0:
                self.period = data.flow.surge_period
            else:
                assert False, 'No period available for the simulation.'

            self.nb_periods = data.solver.nb_periods

            self.dt = round((self.nb_periods * self.period) / self.nb_time_steps, 4)
            self.time = np.arange(0.0, round(self.nb_periods * self.period + self.dt, 4), self.dt)

        else:
            self.period = None
            self.dt = round(self.duration / self.nb_time_steps, 6)
            self.time = np.arange(0.0, round(self.duration + self.dt, 6), self.dt)