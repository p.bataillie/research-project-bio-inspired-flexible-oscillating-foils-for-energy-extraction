import math
# import vpython
import numpy as np
import time


def animate(t, theta, h):
    arrow = vpython.arrow(pos=vpython.vector(-0.75, 0.25, 0), axis=vpython.vector(1, 0, 0),
                          color=vpython.color.orange, shaftwidth=0.01, headlength=0.02, headwidth=0.02, length=0.25)

    arrow_2 = vpython.arrow(pos=vpython.vector(-0.75, -0.25, 0), axis=vpython.vector(1, 0, 0),
                            color=vpython.color.orange, shaftwidth=0.01, headlength=0.02, headwidth=0.02, length=0.25)

    x_axis = vpython.arrow(pos=vpython.vector(-0.5, 0.0, 0), axis=vpython.vector(1, 0, 0),
                           color=vpython.color.red, shaftwidth=0.01, headlength=0.02, headwidth=0.02, length=1.0,
                           height=0.005)

    y_axis = vpython.arrow(pos=vpython.vector(0, 0.5, 0), axis=vpython.vector(0, -1, 0),
                           color=vpython.color.red, shaftwidth=0.01, headlength=0.02, headwidth=0.02, length=1.0,
                           height=0.005)

    vect_normal_airfoil = vpython.vector(np.sin(theta[0]), np.cos(theta[0]), 0)

    airfoil = vpython.box(pos=vpython.vector(0, 0, 0), axis=vpython.vector(1, 0, 0), length=0.5,
                          height=0.005, width=1.0, up=vect_normal_airfoil, color=vpython.color.green)

    i = 0
    while True:
        vpython.rate(25)
        airfoil.pos.y = -h[i]
        airfoil.up = vpython.vector(np.sin(theta[i]), np.cos(theta[i]), 0)
        i = i + 1
        if i >= np.size(t):
            i = 0
        time.sleep(0.025)
