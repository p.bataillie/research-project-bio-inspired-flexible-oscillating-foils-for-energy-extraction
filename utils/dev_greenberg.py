import matplotlib.pyplot as plt

from utils import solver
import numpy as np
import math


solver_obj = solver.Solver(filepath="../tests/cases/test_cases.json", case_set="test_case_greenberg")
solver_obj.solve()
model = solver_obj.output(output_type='model')

greenberg_ref = (1.074
                 - 0.0395 * np.cos(model.w_surge * model.time)
                 + 0.768 * np.sin(model.w_surge * model.time)
                 - 0.074 * np.cos(2.0 * model.w_surge * model.time)
                 - 0.0096 * np.sin(2.0 * model.w_surge * model.time))

l_0 = - 2.0 * math.pi * model.rho * model.b * (model.u_inf ** 2) * model.theta_m

output_model = model.lift_force_total / l_0
output_model_2nd = model.lift_force_test / l_0

plt.figure()
plt.plot(model.time_over_period, output_model, label='Re(xy)')
plt.plot(model.time_over_period, output_model_2nd, label='Re(x)Re(y)')
plt.plot(model.time_over_period, greenberg_ref, '--', label='Test Function')
plt.legend()
plt.grid()
plt.show()

