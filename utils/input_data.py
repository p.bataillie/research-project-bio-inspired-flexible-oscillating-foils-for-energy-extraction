import copy
import json
from dataclasses import dataclass
import numpy as np
import math
import warnings


@dataclass
class Flow:
    freestream_velocity: float
    temperature: float = 288.15
    dynamic_viscosity: float = None
    surge_amplitude: float = 0.0
    surge_frequency: float = 0.0
    surge_period: float = None
    surge_pulsation: float = None
    reynolds_number: float = None
    surge_reduced_frequency: float = None
    rho: float = 1.225

    def __post_init__(self):
        self.dynamic_viscosity = 1.458 * (10 ** (-6)) * ((self.temperature ** (3 / 2)) / (110.4 + self.temperature))

        if self.surge_frequency != 0.0:
            self.surge_period = 1.0 / self.surge_frequency

        self.surge_pulsation = 2.0 * math.pi * self.surge_frequency


@dataclass
class Airfoil:
    chord: float
    aileron_dimension_in_percentage_chord: float
    rotation_axis_in_percentage_chord: float
    critical_lesp: float = None
    aileron_starting_position: float = None
    rotation_axis_position: float = None
    semi_chord: float = None

    def __post_init__(self):
        self.semi_chord = self.chord / 2.0
        self.rotation_axis_position = self.rotation_axis_in_percentage_chord * self.chord - (1 / 2) * self.chord
        self.aileron_starting_position = (1.0 - self.aileron_dimension_in_percentage_chord) * self.chord - (1 / 2) * self.chord


@dataclass
class Movement:
    frequency: float = None

    heaving_phase: float = None
    heaving_amplitude: float = None
    mean_heaving_amplitude: float = None

    pitching_phase: float = None
    pitching_amplitude: float = None
    mean_pitching_amplitude: float = None

    tail_pitching_phase: float = None
    tail_pitching_amplitude: float = None
    mean_tail_pitching_amplitude: float = None

    period: float = None
    angular_frequency: float = None
    reduced_frequency: float = None

    def __post_init__(self):
        if self.frequency is not None:
            if self.frequency == 0.0:
                self.period = 0.0
            else:
                self.period = 1.0 / self.frequency

            self.angular_frequency = 2.0 * math.pi * self.frequency

            self.heaving_phase = np.radians(self.heaving_phase)
            self.pitching_phase = np.radians(self.pitching_phase)
            self.tail_pitching_phase = np.radians(self.tail_pitching_phase)

            self.pitching_amplitude = np.radians(self.pitching_amplitude)
            self.tail_pitching_amplitude = np.radians(self.tail_pitching_amplitude)

            self.mean_pitching_amplitude = np.radians(self.mean_pitching_amplitude)
            self.mean_tail_pitching_amplitude = np.radians(self.mean_tail_pitching_amplitude)


@dataclass
class Solver:
    nb_periods: int = None
    nb_time_steps: int = 250
    duration: float = None


@dataclass
class ExperimentalData:
    lift_coefficient: str = None
    drag_coefficient: str = None
    moment_coefficient: str = None


@dataclass
class Case:
    name: str
    model: str

    flow: Flow
    airfoil: Airfoil
    movement: Movement
    solver: Solver
    experimental_data: ExperimentalData

    def __post_init__(self):
        self.flow = Flow(**self.flow)
        self.airfoil = Airfoil(**self.airfoil)
        self.movement = Movement(**self.movement)
        self.solver = Solver(**self.solver)
        self.experimental_data = ExperimentalData(**self.experimental_data)

        self.flow.reynolds_number = (self.flow.rho * self.flow.freestream_velocity * self.airfoil.chord) / self.flow.dynamic_viscosity

        if self.movement.frequency is not None:
            self.movement.reduced_frequency = (self.movement.angular_frequency * self.airfoil.semi_chord) / self.flow.freestream_velocity

        self.flow.surge_reduced_frequency = (self.flow.surge_pulsation * self.airfoil.semi_chord) / self.flow.freestream_velocity


@dataclass
class InputData:
    case: Case

    def __post_init__(self):
        self.case = Case(**self.case)


def theodorsen_garrick_scaling(data):
    real_chord = data.case.airfoil.chord

    # Airfoil Quantities Scaling
    data.case.airfoil.chord = 2.0
    data.case.airfoil.semi_chord = data.case.airfoil.chord / 2.0

    data.case.airfoil.rotation_axis_position = data.case.airfoil.rotation_axis_in_percentage_chord * data.case.airfoil.chord - (1.0 / 2.0) * data.case.airfoil.chord
    data.case.airfoil.aileron_starting_position = (1.0 - data.case.airfoil.aileron_dimension_in_percentage_chord) * data.case.airfoil.chord - (1.0 / 2.0) * data.case.airfoil.chord

    # Movement Quantities Scaling
    data.case.movement.angular_frequency = (data.case.movement.reduced_frequency * data.case.flow.freestream_velocity) / data.case.airfoil.semi_chord
    data.case.movement.frequency = data.case.movement.angular_frequency / (2.0 * math.pi)

    if data.case.movement.frequency == 0.0:
        data.case.movement.period = 0.0
    else:
        data.case.movement.period = 1.0 / data.case.movement.frequency

    data.case.movement.heaving_amplitude = (data.case.movement.heaving_amplitude * data.case.airfoil.chord) / real_chord
    data.case.movement.mean_heaving_amplitude = (data.case.movement.mean_heaving_amplitude * data.case.airfoil.chord) / real_chord

    return data


def load_data(filepath, case_set, scaling=True):
    data_file = open(filepath)
    json_file = data_file.read()
    data_dict = json.loads(json_file)

    initial_data = []

    for i in range(len(data_dict[case_set])):
        initial_data.append(InputData(data_dict[case_set][i]))

    return initial_data
