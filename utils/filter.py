import scipy
from utils import array
import numpy as np

def lowpass_filter(data, cutoff, sample_rate, poles = 5):
    y = array.remove_nan(data)
    sos = scipy.signal.butter(poles, cutoff, btype='lowpass', fs=sample_rate, analog=False, output='sos')
    filtered_data = scipy.signal.sosfiltfilt(sos, y)
    if np.size(filtered_data) != np.size(data):
        filtered_data = array.reshape_with_nan(filtered_data, data)
    return filtered_data
