import numpy as np

def remove_nan(arr):
    return arr[~np.isnan(arr)]

def reshape_with_nan(arr, arr_ref):
    for i in range(np.size(arr_ref) - np.size(arr)):
        arr = np.insert(arr, 0, np.nan)
    return arr