import numpy as np
from utils import solver

def func(time):
    pitching_amplitude = 20.0
    heaving_amplitude = 0.1

    angular_pulsation = 20.0

    theta = np.radians(pitching_amplitude) * np.cos(angular_pulsation * time)
    h = heaving_amplitude * np.sin(angular_pulsation * time)

    return theta, h

solver_obj = solver.Solver(filepath="cases/experimental.json", case_set="ldvm_test")
solver_obj.solve(func=func)
results = solver_obj.output()

# results.show_movement(show_plot=False)

data = [[[results.time, results.lift_coefficient, 'num', 'LDVM - NACA0015', 'green']],
        [[results.time, results.drag_coefficient, 'num', 'LDVM - NACA0015', 'green']]]

axis_labels = [['$t$', '$C_{L}$'], ['$\\frac{t}{T}$', '$C_{D}$']]

results.show_lift_drag_general(data,
                               axis_labels,
                               show_plot=True)
