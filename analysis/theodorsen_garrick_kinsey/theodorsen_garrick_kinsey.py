from utils import solver
import os

case_set = "kinsey"

os.chdir("../..")
analysis_path = "analysis/theodorsen_garrick_kinsey/"
case_filepath = analysis_path + "cases/kinsey.json"
figures_path = analysis_path + "figures/" + case_set + "/"

solver_obj = solver.Solver(filepath=case_filepath, case_set=case_set)
solver_obj.solve()
results = solver_obj.output()

if not os.path.exists(figures_path):
    os.mkdir(figures_path)
else:
    for f in os.listdir(figures_path):
        os.remove(figures_path + f)

if isinstance(results, list):
    for i in range(len(results)):
        results[i].show_movement(show_plot=False)
        results[i].show_lift_drag(save_figure=True, figures_path=figures_path, lift_coefficient_range=[-1.6, 1.2],
                                  drag_coefficient_range=[-0.065, 0.06])
        results[i].show_moment_theta(save_figure=True, figures_path=figures_path, moment_coefficient_range=[-0.06, 0.09])
else:
    results.show_infos()
    results.show_movement(show_plot=False)
    results.show_lift_drag(save_figure=True, figures_path=figures_path)
    results.show_moment_theta(save_figure=True, figures_path=figures_path)
