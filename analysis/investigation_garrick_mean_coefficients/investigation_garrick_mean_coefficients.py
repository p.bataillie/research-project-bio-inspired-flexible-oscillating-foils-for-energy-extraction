import math

from utils import solver
import numpy as np


def drag_s_complex_real():
    solver_obj = solver.Solver(filepath="../../tests/cases/test_cases.json", case_set="test_case_garrick")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')

# ------------------- #
    time_averaged_drag_from_airfoil_with_coefficients = math.pi * model.rho * model.b * (model.w ** 2) * (model.b2 * (model.theta_0 ** 2)
                                                                                                                       + 2.0 * model.b4 * model.theta_0 * model.h_0
                                                                                                                       + 2.0 * model.b6 * model.theta_0 * model.delta_0)

    time_averaged_drag_from_airfoil_with_integral = (model.w / (2.0 * math.pi)) * np.trapz(model.theta.imag * model.lift_force_airfoil_real, x=model.time)

    print('')
    print('time_averaged_drag_from_airfoil_with_coefficients = ' + str(time_averaged_drag_from_airfoil_with_coefficients))
    print('time_averaged_drag_from_airfoil_with_integral = ' + str(time_averaged_drag_from_airfoil_with_integral))
    print('It means that b6 is accurate.')

# ------------------- #
    time_averaged_drag_from_aileron_with_coefficients = math.pi * model.rho * model.b * (model.w ** 2) * (model.c3 * (model.delta_0 ** 2)
                                                                                                          + 2.0 * model.c5 * model.delta_0 * model.h_0
                                                                                                          + 2.0 * model.c6 * model.theta_0 * model.delta_0)

    time_averaged_drag_from_aileron_with_integral = (model.w / (2.0 * math.pi)) * np.trapz(model.delta.imag * model.lift_force_aileron_complex.imag, x=model.time)

    value_c6_would_need_to_have = ((time_averaged_drag_from_aileron_with_integral / (math.pi * model.rho * model.b * (model.w ** 2))) - (model.c3 * (model.delta_0 ** 2) + 2.0 * model.c5 * model.delta_0 * model.h_0)) / (2.0 * model.theta_0 * model.delta_0)

    print('')
    print('time_averaged_drag_from_aileron_with_coefficients = ' + str(time_averaged_drag_from_aileron_with_coefficients))
    print('time_averaged_drag_from_aileron_with_integral = ' + str(time_averaged_drag_from_aileron_with_integral))
    print('It means that c6 is wrong (because model.delta.imag * model.lift_force_aileron.imag has been validated in Theodorsen).')

# ------------------- #
    time_averaged_drag_from_s_with_coefficients = math.pi * model.rho * model.b * (model.w ** 2) * (model.a1 * (model.h_0 ** 2)
                                                                                                    + model.a2 * (model.theta_0 ** 2)
                                                                                                    + model.a3 * (model.delta_0 ** 2)
                                                                                                    + 2.0 * model.a4 * model.theta_0 * model.h_0
                                                                                                    + 2.0 * model.a5 * model.delta_0 * model.h_0
                                                                                                    + 2.0 * model.a6 * model.theta_0 * model.delta_0)

    time_averaged_drag_from_s_with_integral = (model.w / (2.0 * math.pi)) * np.trapz(model.rho * math.pi * (model.S.imag ** 2), x=model.time)
    time_averaged_drag_from_s_with_coefficients_M_N = ((math.pi * model.rho) / 4.0) * ((model.M_real ** 2) + (model.N_real ** 2))

    value_a6_would_need_to_have = (time_averaged_drag_from_s_with_integral / (math.pi * model.rho * model.b * (model.w ** 2)) - (model.a1 * (model.h_0 ** 2)
                                                                    + model.a2 * (model.theta_0 ** 2)
                                                                    + model.a3 * (model.delta_0 ** 2)
                                                                    + 2.0 * model.a4 * model.theta_0 * model.h_0
                                                                    + 2.0 * model.a5 * model.delta_0 * model.h_0)) / (2.0 * model.theta_0 * model.delta_0)

    print('')
    print('time_averaged_drag_from_s_with_coefficients = ' + str(time_averaged_drag_from_s_with_coefficients))
    print('time_averaged_drag_from_s_with_coefficients_M_N = ' + str(time_averaged_drag_from_s_with_coefficients_M_N))
    print('time_averaged_drag_from_s_with_integral = ' + str(time_averaged_drag_from_s_with_integral))
    print('model.S.imag has already been validated anyway, another proof that model.S.imag is accurate.')
    print('It means that a6 is wrong.')

# ------------------- #
    time_averaged_drag_with_coefficients_1st = model.time_averaged_drag_coefficients
    time_averaged_drag_with_coefficients_2nd = model.time_averaged_drag_coefficients_2nd
    time_averaged_drag_with_integral = (model.w / (2.0 * math.pi)) * np.trapz(model.px_real, x=model.time)

    value_A6_would_need_to_have = ((time_averaged_drag_with_integral / (math.pi * model.rho * model.b * (model.w ** 2))) - (model.A1 * (model.h_0 ** 2)
                                                                             + model.A2 * (model.theta_0 ** 2)
                                                                             + model.A3 * (model.delta_0 ** 2)
                                                                             + 2.0 * model.A4 * model.theta_0 * model.h_0
                                                                             + 2.0 * model.A5 * model.delta_0 * model.h_0)) / (2.0 * model.theta_0 * model.delta_0)

    print('')
    print('time_averaged_drag_with_coefficients_1st = ' + str(time_averaged_drag_with_coefficients_1st))
    print('time_averaged_drag_with_coefficients_2nd = ' + str(time_averaged_drag_with_coefficients_2nd))
    print('time_averaged_drag_with_integral = ' + str(time_averaged_drag_with_integral))


drag_s_complex_real()