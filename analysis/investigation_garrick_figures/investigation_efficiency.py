import matplotlib.pyplot as plt
import numpy as np
import os
from utils import solver

os.chdir("../..")

solver_obj = solver.Solver(filepath="analysis/investigation_garrick_figures/cases/figure_3.json", case_set="figure_3")
solver_obj.solve()
results = solver_obj.output()

if isinstance(results, list):
    reduced_frequency_inv = np.zeros(len(results))
    efficiency = np.zeros(len(results))
    efficiency_2nd = np.zeros(len(results))

    for i in range(len(results)):
        reduced_frequency_inv[i] = 1.0 / results[i].model.k
        efficiency[i] = results[i].model.efficiency_garrick_coefficients
        efficiency_2nd[i] = results[i].model.efficiency_garrick_coefficients_2nd


garrick_data = csv_tools.read_csv('analysis/investigation_garrick_figures/experimental_data/figure_3.csv', delimiter=',', skip=1)

plt.figure()
plt.plot(garrick_data[:, 0], garrick_data[:, 1], color='black', label='Garrick')
plt.plot(reduced_frequency_inv, efficiency, label='1st Method')
plt.plot(reduced_frequency_inv, efficiency_2nd, '--', label='2nd Method')
plt.xlabel('Reduced Pulsation $\\frac{1}{k}$')
plt.ylabel('Efficiency $\\frac{\\overline{P_{x}}U}{\\overline{W}}$')
plt.grid()
plt.legend()
plt.ylim([0.0, 1.0])
plt.savefig('analysis/investigation_garrick_figures/figures/figure_3.svg')

solver_obj = solver.Solver(filepath="analysis/investigation_garrick_figures/cases/figure_4.json", case_set="a_0")
solver_obj.solve()
results = solver_obj.output()

if isinstance(results, list):
    reduced_frequency_inv = np.zeros(len(results))
    qte_a_0 = np.zeros(len(results))

    for i in range(len(results)):
        reduced_frequency_inv[i] = 1.0 / results[i].model.k
        qte_a_0[i] = results[i].model.time_averaged_ke_coefficients / results[i].model.time_averaged_total_work_coefficients

solver_obj = solver.Solver(filepath="analysis/investigation_garrick_figures/cases/figure_4.json", case_set="a_0_5")
solver_obj.solve()
results = solver_obj.output()

if isinstance(results, list):
    qte_a_0_5 = np.zeros(len(results))

    for i in range(len(results)):
        reduced_frequency_inv[i] = 1.0 / results[i].model.k
        qte_a_0_5[i] = results[i].model.time_averaged_ke_coefficients / results[i].model.time_averaged_total_work_coefficients

solver_obj = solver.Solver(filepath="analysis/investigation_garrick_figures/cases/figure_4.json", case_set="a_1")
solver_obj.solve()
results = solver_obj.output()

if isinstance(results, list):
    qte_a_1 = np.zeros(len(results))

    for i in range(len(results)):
        reduced_frequency_inv[i] = 1.0 / results[i].model.k
        qte_a_1[i] = results[i].model.time_averaged_ke_coefficients / results[i].model.time_averaged_total_work_coefficients

solver_obj = solver.Solver(filepath="analysis/investigation_garrick_figures/cases/figure_4.json", case_set="a_n_0_5")
solver_obj.solve()
results = solver_obj.output()

if isinstance(results, list):
    qte_a_n_0_5 = np.zeros(len(results))

    for i in range(len(results)):
        reduced_frequency_inv[i] = 1.0 / results[i].model.k
        qte_a_n_0_5[i] = results[i].model.time_averaged_ke_coefficients / results[i].model.time_averaged_total_work_coefficients

solver_obj = solver.Solver(filepath="analysis/investigation_garrick_figures/cases/figure_4.json", case_set="a_n_1")
solver_obj.solve()
results = solver_obj.output()

if isinstance(results, list):
    qte_a_n_1 = np.zeros(len(results))

    for i in range(len(results)):
        reduced_frequency_inv[i] = 1.0 / results[i].model.k
        qte_a_n_1[i] = results[i].model.time_averaged_ke_coefficients / results[i].model.time_averaged_total_work_coefficients

# garrick_data = csv_tools.read_csv('experimental_data/investigation_efficiency/figure_3.csv', delimiter=',', skip=1)

plt.figure()
# plt.plot(garrick_data[:, 0], garrick_data[:, 1], color='black', label='Garrick')
plt.plot(reduced_frequency_inv, qte_a_0, label='a = 0.0')
plt.plot(reduced_frequency_inv, qte_a_0_5, label='a = 0.5')
plt.plot(reduced_frequency_inv, qte_a_1, label='a = 1.0')
plt.plot(reduced_frequency_inv, qte_a_n_0_5, label='a = - 0.5')
plt.plot(reduced_frequency_inv, qte_a_n_1, label='a = - 1.0')

plt.xlabel('Reduced Pulsation $\\frac{1}{k}$')
plt.ylabel('$\\frac{\\overline{E}}{\\overline{W}}$')
plt.grid()
plt.legend()
plt.xlim([0.0, 4.0])
plt.ylim([0.0, 5.0])
plt.savefig('analysis/investigation_garrick_figures/figures/figure_4.svg')
plt.show()
