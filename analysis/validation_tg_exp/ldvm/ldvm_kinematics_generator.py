import numpy as np

from utils import solver
import os

case_set = "pitching_heaving"

os.chdir("../../..")
analysis_path = "analysis/validation_tg_exp/"
case_filepath = analysis_path + "cases/experiments_braud_ferrand.json"
figures_path = analysis_path + "figures/" + case_set + "/"

solver_obj = solver.Solver(filepath=case_filepath, case_set=case_set)
solver_obj.solve()
results = solver_obj.output()

if not os.path.exists(figures_path):
    os.mkdir(figures_path)
else:
    for f in os.listdir(figures_path):
        os.remove(figures_path + f)

if isinstance(results, list):
    for i in range(len(results)):
    # for i in range(len([1])):
        if results[i].name == "f_0_8-pitching_amplitude_8":
            print(results[i].name)
            results[i].show_movement(show_plot=False)
            export = np.zeros((len(results[i].time), 4), dtype=np.float32)
            export[:, 0] = np.round((results[i].time * results[i].u_inf) / results[i].chord, 8)
            export[:, 1] = np.round(np.degrees(results[i].theta), 8)
            export[:, 2] = np.round(- results[i].h / results[i].chord, 8) * 0.0
            for o in range(len(results[i].time)):
                export[o, 3] = np.round(1.0, 8)
            export = np.round(export, 8)
            np.savetxt("analysis/validation_tg_exp/ldvm/inputs_" + results[i].name + ".dat", export, delimiter="   ", fmt='%.7e', newline="\n   ")


