import matplotlib.pyplot as plt
import numpy as np

from utils import solver
import os

def plot_2d(axs=None, data=None, data2=None, experimental_data=None, axis_labels=None, curve_labels=None, x_range=None,
            y_range=None):
    if curve_labels is None:
        axs.plot(data[0], data[1], color='red')
        axs.plot(data2[0], data2[1], color='green')
        if experimental_data is not None:
            axs.scatter(experimental_data[0], experimental_data[1], color='black', s=8)
    else:
        axs.plot(data[0], data[1], label=curve_labels[0], color='red')
        axs.plot(data2[0], data2[1], label=curve_labels[1], color='green')
        if experimental_data is not None:
            axs.scatter(experimental_data[0], experimental_data[1], label=curve_labels[2], color='black',
                        s=8)
    axs.grid()
    if x_range is not None:
        axs.set_xlim(left=x_range[0], right=x_range[1])
    if y_range is not None:
        axs.set_ylim(bottom=y_range[0], top=y_range[1])
    if curve_labels is not None:
        axs.legend()
    if axis_labels is not None:
        axs.set_xlabel(axis_labels[0])
        axs.set_ylabel(axis_labels[1])

case_set = "pitching_heaving"

os.chdir("../../..")
analysis_path = "analysis/validation_tg_exp/"
case_filepath = analysis_path + "cases/experiments_braud_ferrand.json"
figures_path = analysis_path + "figures/" + case_set + "/"

solver_obj = solver.Solver(filepath=case_filepath, case_set=case_set)
solver_obj.solve()
results = solver_obj.output()

if not os.path.exists(figures_path):
    os.mkdir(figures_path)
else:
    for f in os.listdir(figures_path):
        os.remove(figures_path + f)

if isinstance(results, list):
    for i in range(len(results)):
        # if results[i].name == 'f_1_6-pitching_amplitude_8':
            file_data = np.loadtxt("analysis/validation_tg_exp/ldvm/outputs/outputs_" + results[i].name + ".dat")
            # file_databis = np.loadtxt("analysis/validation_tg_exp/ldvm/outputs_f_1_6-pitching_amplitude_8bis.dat")

            skip = 0

            time = file_data[skip:, 0] / results[i].period
            lift = -file_data[skip:, 8]
            drag = file_data[skip:, 9]
            moment = file_data[skip:, 10]

            # timebis = file_databis[:, 0] / results[i].period
            # liftbis = -file_databis[:, 8]
            # dragbis = file_databis[:, 9]
            # momentbis = file_databis[:, 10]

            fig, axs = plt.subplots(1, 2, figsize=(12, 5))
            plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9, wspace=0.4, hspace=0.3)

            plot_2d(axs=axs[0],
                    data=[results[i].time_over_period, results[i].lift_coefficient],
                    data2=[time, lift],
                    experimental_data=[results[i].time_over_period_exp, results[i].lift_coefficient_exp],
                    axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{L}$'],
                    curve_labels=['Theodorsen - FP', 'LDVM - NACA0015', 'Exp - NACA0015'],
                    y_range=None)
            plot_2d(axs=axs[1],
                    data=[results[i].time_over_period, results[i].drag_coefficient],
                    data2=[time, drag],
                    experimental_data=[results[i].time_over_period_exp, results[i].drag_coefficient_exp],
                    axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{D}$'],
                    curve_labels=['Garrick - FP', 'LDVM - NACA0015', 'Exp - NACA0015'],
                    y_range=None)

            plt.suptitle('$U_{\\infty} = $' + str(round(results[i].u_inf, 2)) + ' $m.s^{-1}$ | $f^{*}$ = ' + str(
                round(results[i].f_star, 4)) + ' | $\\theta_{0}$ = ' + str(
                round(np.degrees(results[i].pitching_amplitude), 2)) + '$^{\\circ}$ | $h_{0}$/c = ' + str(
                round(results[i].heaving_amplitude / results[i].chord, 2)) + ' | $x_{p}$/c = ' + str(
                round(results[i].rotation_axis_in_percentage_chord, 2)), fontsize=10.5)


            fig.savefig("analysis/validation_tg_exp/ldvm/outputs/outputs_ld_" + results[i].name + ".svg")

            fig, axs = plt.subplots(1, 1)
            # plt.subplots_adjust(left=0.1, bottom=0.15, right=0.9, top=0.9, wspace=0.4, hspace=0.3)

            plot_2d(axs=axs,
                    data=[results[i].time_over_period, results[i].moment_airfoil_coefficient],
                    data2=[time, moment],
                    experimental_data=[results[i].time_over_period_exp, results[i].moment_coefficient_exp],
                    axis_labels=['Dimensionless Time $\\frac{t}{T}$', '$C_{m_{\\theta}}$'],
                    curve_labels=['Theodorsen - FP', 'LDVM - NACA0015', 'Exp - NACA0015'],
                    y_range=None)

            axs.set_title('$U_{\\infty} = $' + str(round(results[i].u_inf, 2)) + ' $m.s^{-1}$ | $f^{*}$ = ' + str(
                round(results[i].f_star, 4)) + ' | $\\theta_{0}$ = ' + str(
                round(np.degrees(results[i].pitching_amplitude), 2)) + '$^{\\circ}$ | $h_{0}$/c = ' + str(
                round(results[i].heaving_amplitude / results[i].chord, 2)) + ' | $x_{p}$/c = ' + str(
                round(results[i].rotation_axis_in_percentage_chord, 2)), fontsize=10)

            fig.savefig("analysis/validation_tg_exp/ldvm/outputs/outputs_m_" + results[i].name + ".svg")

            # plt.show()

