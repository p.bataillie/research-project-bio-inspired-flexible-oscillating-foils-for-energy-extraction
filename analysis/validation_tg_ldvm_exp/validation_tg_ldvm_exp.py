from utils import solver
import os

case_set = "pitching_heaving"

os.chdir("../..")
analysis_path = "analysis/validation_tg_ldvm_exp/"
case_filepath = analysis_path + "cases/validation_tg_ldvm_exp.json"
figures_path = analysis_path + "figures/" + case_set + "/"

if not os.path.exists(figures_path):
    os.mkdir(figures_path)
else:
    for f in os.listdir(figures_path):
        os.remove(figures_path + f)

cases = ["f_1_6-pitching_amplitude_2",
         "f_1_6-pitching_amplitude_4",
         "f_1_6-pitching_amplitude_6",
         "f_1_6-pitching_amplitude_7",
         "f_1_6-pitching_amplitude_8",
         "f_0_8-pitching_amplitude_2",
         "f_0_8-pitching_amplitude_4",
         "f_0_8-pitching_amplitude_6",
         "f_0_8-pitching_amplitude_8",
         "f_0_8-pitching_amplitude_10"]

for i in range(len(cases)):
    print(cases[i])

    solver_obj = solver.Solver(filepath=case_filepath, case_set=cases[i])
    solver_obj.solve()
    results = solver_obj.output()

    data_lift_drag = [[[results[0].time_over_period, results[0].lift_coefficient, 'num', 'Theodorsen - FP', 'red'], [results[1].time_over_period, results[1].lift_coefficient, 'num', 'LDVM - NACA0015', 'green'], [results[0].time_over_period_exp, results[0].lift_coefficient_exp, 'exp', 'A. Braud, V. Ferrand (2021) - FP', 'black']],
                      [[results[0].time_over_period, results[0].drag_coefficient, 'num', 'Garrick - FP', 'red'], [results[1].time_over_period, results[1].drag_coefficient, 'num', 'LDVM - NACA0015', 'green'], [results[0].time_over_period_exp, results[0].drag_coefficient_exp, 'exp', 'A. Braud, V. Ferrand (2021) - FP', 'black']]]

    if cases[i] == "f_0_8-pitching_amplitude_10":
        y_range = [[-1.6, 1.2], [-0.065, 0.17]]
    else:
        y_range = [[-1.6, 1.2], [-0.065, 0.06]]

    axis_labels = [['$\\frac{t}{T}$', '$C_{L}$'], ['$\\frac{t}{T}$', '$C_{D}$']]

    results[0].show_lift_drag_general(data_lift_drag,
                                      axis_labels,
                                      figures_path=figures_path,
                                      show_plot=False,
                                      save_figure=True,
                                      y_range=y_range)

    data_moment = [[[results[0].time_over_period, results[0].moment_airfoil_coefficient, 'num', 'Theodorsen - FP', 'red'],
                   [results[1].time_over_period, results[1].moment_airfoil_coefficient, 'num', 'LDVM - NACA0015', 'green'],
                   [results[0].time_over_period_exp, results[0].moment_coefficient_exp, 'exp', 'A. Braud, V. Ferrand (2021) - FP',
                    'black']]]

    y_range = [[-0.06, 0.09]]

    axis_labels = [['$\\frac{t}{T}$', '$C_{m_{\\theta}}$']]

    results[0].show_moment_theta_general(data_moment,
                                         axis_labels,
                                         figures_path=figures_path,
                                         show_plot=False,
                                         save_figure=True,
                                         y_range=y_range)