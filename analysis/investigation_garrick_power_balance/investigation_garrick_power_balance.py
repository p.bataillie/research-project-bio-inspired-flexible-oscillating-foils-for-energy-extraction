import matplotlib.pyplot as plt
import numpy as np
from utils import solver

solver_obj = solver.Solver(filepath="../../cases/experimental.json", case_set="Garrick")
solver_obj.solve()
results = solver_obj.output()

print("Feathering Parameter: " + str(results.feathering_parameter))

print("Required Pitching Energy: " + str(results.pitching_energy_control) + " J.")
print("Available Heaving Energy: " + str(results.heaving_energy_control) + " J.")

print("Paper: " + str(results.efficiency))
print("Garrick: " + str(results.model.efficiency_garrick_coefficients))

# results.show_movement(debug=True)
# results.show_lift_drag()
# results.show_power()
# results.show_moments()

# animation.animate(results.time, results.theta, results.h)

plt.figure()
plt.plot(results.time_over_period, results.drag_power, label='Drag Power')
plt.plot(results.time_over_period, results.heaving_power, label='Heaving Power')
plt.plot(results.time_over_period, results.pitching_power, label='Pitching Power')
plt.plot(results.time_over_period, (results.heaving_power + results.pitching_power), label='Heaving + Pitching Power')
plt.grid()
plt.legend()
plt.xlabel('Dimensionless Time $\\frac{t}{T}$')
plt.ylabel('Power ($W$)')
plt.title('$\\theta_{0}$ = ' + str(np.degrees(results.pitching_amplitude)) + '$^{\\circ}$')
plt.savefig('figures/power_balance_' + str(np.degrees(results.pitching_amplitude)) + '.svg')
plt.show()
