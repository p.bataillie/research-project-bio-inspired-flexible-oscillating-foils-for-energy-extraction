import matplotlib.pyplot as plt
import numpy as np
from opti import opti_tools


optim = opti_tools.Optimisation("cases/cases.json", "Garrick")

heaving_percentage = np.arange(0.025, 0.6 + 0.025, 0.025)
xp = np.arange(0.025, 0.6 + 0.025, 0.025)

u_inf = optim.data_dict[optim.case_set][0]['flow']['freestream_velocity']
f_star = optim.data_dict[optim.case_set][0]['movement']['frequency'] * ((optim.data_dict[optim.case_set][0]['airfoil']['chord'] / 2.0) / u_inf)
theta_0 = optim.data_dict[optim.case_set][0]['movement']['pitching_amplitude']

size = (np.size(xp), np.size(heaving_percentage))
feathering_parameters = np.zeros(size)
aerodynamic_efficiencies = np.zeros(size)

for i in range(np.size(xp)):
    for j in range(np.size(heaving_percentage)):
        print('Case: ' + str(i) + ' | ' + str(j))
        res = optim.solve(xp=xp[i], heaving_percentage=heaving_percentage[j])
        feathering_parameters[i][j] = res.feathering_parameter
        if res.feathering_parameter > 1.0 and res.efficiency >= 0.0:
            aerodynamic_efficiencies[i][j] = res.efficiency
        else:
            aerodynamic_efficiencies[i][j] = np.nan

print(feathering_parameters)
print(aerodynamic_efficiencies)


X, Y = np.meshgrid(xp, heaving_percentage, indexing='ij')

plt.figure()
contour = plt.contourf(X, Y, aerodynamic_efficiencies, levels=40)
plt.colorbar(contour, label='$\\eta$')
plt.xlabel('$x_{p}$/c')
plt.ylabel('$h_{0}$/c')
plt.grid()
plt.title('$U_{\\infty} = $' + str(u_inf) + ' $m.s^{-1}$ | f*= ' + str(f_star) + ' | $\\theta_{0}$ = ' + str(theta_0) + '$^{\\circ}$', fontsize=10)

plt.savefig('figures/xp_heaving_graph.svg')

plt.show()



