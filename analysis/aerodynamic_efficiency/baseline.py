from utils import solver

solver_obj = solver.Solver(filepath="cases/cases.json", case_set="Garrick")
solver_obj.solve()
results = solver_obj.output()

results.show_infos()

# results.show_movement(debug=True)
# results.show_lift_drag()
# results.show_power()
# results.show_moments()

# animation.animate(results.time, results.theta, results.h)
