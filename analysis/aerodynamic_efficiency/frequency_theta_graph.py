import matplotlib.pyplot as plt
import numpy as np

from opti import opti_tools


optim = opti_tools.Optimisation("cases/cases.json", "Garrick")

theta_0 = np.arange(0.0, 91.0, 1.0)
freq_star = np.arange(0.002, 0.11 + 0.002, 0.002)
freq_star = freq_star[1:]

size = (np.size(freq_star), np.size(theta_0))
feathering_parameters = np.zeros(size)
aerodynamic_efficiencies = np.zeros(size)
froude_efficiency = np.zeros(size)
feathering_param_vect = np.zeros(np.size(freq_star))

u_inf = optim.data_dict[optim.case_set][0]['flow']['freestream_velocity']
h0_c = optim.data_dict[optim.case_set][0]['movement']['heaving_amplitude'] / optim.data_dict[optim.case_set][0]['airfoil']['chord']
xp = optim.data_dict[optim.case_set][0]['airfoil']['rotation_axis_in_percentage_chord']

for i in range(np.size(freq_star)):
    print('Case: ' + str(i) + ' / ' + str(np.size(freq_star)))
    for j in range(np.size(theta_0)):
        res = optim.solve(frequency_dimensionless=freq_star[i], pitching_amplitude=theta_0[j])
        feathering_param_vect[i] = np.degrees(np.arctan((res.heaving_amplitude * res.angular_frequency) / res.u_inf))
        feathering_parameters[i][j] = res.feathering_parameter
        if res.efficiency >= 0.0:
            aerodynamic_efficiencies[i][j] = res.efficiency
        else:
            aerodynamic_efficiencies[i][j] = np.nan
        if res.feathering_parameter > 1.0:
            froude_efficiency[i][j] = res.froude_efficiency
        else:
            froude_efficiency[i][j] = np.nan

print(feathering_parameters)
print(aerodynamic_efficiencies)


X, Y = np.meshgrid(freq_star, theta_0, indexing='ij')

plt.figure()
contour = plt.contourf(X, Y, aerodynamic_efficiencies, vmin=0.0, levels=12)
plt.plot(freq_star, feathering_param_vect, color='red', label='$\\chi = 1$')
plt.colorbar(contour, label='$\\eta$')
plt.xlabel('$f^{*}$')
plt.ylabel('$\\theta_{0}$ ($^{\\circ}$)')
plt.grid()
plt.title('$U_{\\infty} = $' + str(round(u_inf, 2)) + ' $m.s^{-1}$ | $h_{0}$/c = ' + str(h0_c) + ' | $x_{p}$/c = ' + str(xp), fontsize=10)
plt.legend(loc='lower right')
plt.savefig('figures/frequency_theta_graph_kinsey.svg')

plt.show()

# plt.figure()
# contour = plt.contourf(X, Y, froude_efficiency, vmin=0.0, levels=12)
# plt.colorbar(contour, label='$\\eta$')
# plt.xlabel('$f^{*}$')
# plt.ylabel('$\\theta_{0}$ ($^{\\circ}$)')
# plt.grid()
# plt.title('$U_{\\infty} = $' + str(u_inf) + ' $m.s^{-1}$ | $h_{0}$/c = ' + str(h0_c) + ' | $x_{p}$/c = ' + str(xp), fontsize=10)
#
# plt.savefig('figures/frequency_theta_graph_froude_efficiency.svg')
#
# plt.show()



