import numpy as np

from opti import opti_tools


optim = opti_tools.Optimisation("cases/cases.json", "Garrick")

res = optim.solve(frequency_dimensionless=0.01)

res.show_infos()
res.show_movement(debug=True)
# res.show_power()
# res.show_moment_theta()
# res.show_lift_drag(show_plot=True)

print(np.mean(res.power_output))

# animation.animate(res.time, res.theta, res.h)




