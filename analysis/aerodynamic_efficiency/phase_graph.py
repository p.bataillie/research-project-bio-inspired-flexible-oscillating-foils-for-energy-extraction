import matplotlib.pyplot as plt
import numpy as np

from opti import opti_tools


optim = opti_tools.Optimisation("cases/cases.json", "Garrick")

pitching_phase = np.arange(0.0, 190.0, 5.0)
heaving_phase = np.arange(0.0, 370.0, 5.0)

size = (np.size(heaving_phase), np.size(pitching_phase))
feathering_parameters = np.zeros(size)
aerodynamic_efficiencies = np.zeros(size)

u_inf = optim.data_dict[optim.case_set][0]['flow']['freestream_velocity']
h0_c = optim.data_dict[optim.case_set][0]['movement']['heaving_amplitude'] / optim.data_dict[optim.case_set][0]['airfoil']['chord']
f_star = optim.data_dict[optim.case_set][0]['movement']['frequency'] * ((optim.data_dict[optim.case_set][0]['airfoil']['chord'] / 2.0) / u_inf)
xp = optim.data_dict[optim.case_set][0]['airfoil']['rotation_axis_in_percentage_chord']
theta_0 = optim.data_dict[optim.case_set][0]['movement']['pitching_amplitude']

for i in range(np.size(heaving_phase)):
    print('Case: ' + str(i))
    for j in range(np.size(pitching_phase)):
        res = optim.solve(heaving_phase=heaving_phase[i], pitching_phase=pitching_phase[j])
        feathering_parameters[i][j] = res.feathering_parameter
        if res.efficiency >= 0.0:
            aerodynamic_efficiencies[i][j] = res.efficiency
        else:
            aerodynamic_efficiencies[i][j] = np.nan

print(feathering_parameters)
print(aerodynamic_efficiencies)


X, Y = np.meshgrid(heaving_phase, pitching_phase, indexing='ij')

plt.figure()
contour = plt.contourf(X, Y, aerodynamic_efficiencies, levels=40)
plt.colorbar(contour, label='$\\eta$')
plt.xlabel('Heaving Phase $\\phi_{2}$ ($^{\\circ}$)')
plt.ylabel('Pitching Phase $\\phi_{0}$ ($^{\\circ}$)')
plt.grid()
plt.title('$U_{\\infty} = $' + str(u_inf) + ' $m.s^{-1}$ | $h_{0}$/c = ' + str(h0_c) + ' | f*=' + str(f_star) + ' | $x_{p}$/c = ' + str(xp) + ' | $\\theta_{0}$ = ' + str(theta_0) + '$^{\\circ}$', fontsize=10)

# plt.savefig('figures/phase_graph_uinf_' + str(u_inf) + '_h0c_' + str(h0_c) + '.svg')
plt.savefig('figures/phase_graph.svg')

plt.show()

