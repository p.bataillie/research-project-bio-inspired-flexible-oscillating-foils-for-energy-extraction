import os
import numpy as np
from matplotlib import pyplot as plt
from analysis.filtering_ldvm import fft_tools
from utils import solver, array, filter
import matplotlib

os.chdir('../../')

case = "lift_64_deg_sym"

analysis_path = 'analysis/validation_ldvm/'
experimental_data_path = analysis_path + 'experimental_data/'
cases_path = analysis_path + 'cases/experimental.json'
data_exp = np.load(experimental_data_path + case + '/' + case + '.npy')
figures_path = analysis_path + 'figures/frequency/' + case

def func(time):
    time_arr = data_exp[:, 0]
    theta_arr = data_exp[:, 1]

    theta = np.radians(theta_arr[np.abs(time_arr - time).argmin()])
    h = 0.0

    return theta, h

solver_obj_1 = solver.Solver(filepath=cases_path, case_set="ldvm_test_with_ldvm")
solver_obj_1.solve(func=func)
results_1 = solver_obj_1.output()

cl_filtered = filter.lowpass_filter(results_1.lift_coefficient, 7.5, sample_rate=(1.0 / results_1.time_step))
cd_filtered = filter.lowpass_filter(results_1.drag_coefficient, 7.5, sample_rate=(1.0 / results_1.time_step))

period = results_1.model.time_manager.duration

time_exp = np.linspace(data_exp[:, 0].min(), data_exp[:, 0].max(), 65)
cl_exp = np.interp(time_exp, data_exp[:, 0], data_exp[:, 2])

data = [[[results_1.time / period, results_1.lift_coefficient, 'num', 'LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018', 'green'], [results_1.time / period, cl_filtered, 'num', 'LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018 - Filtered', 'blue']],
        [[results_1.time / period, results_1.drag_coefficient, 'num', 'LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018', 'green'], [results_1.time / period, cd_filtered, 'num', 'LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018 - Filtered', 'blue']]]

axis_labels = [['$\\frac{t}{T}$', '$C_{L}$'], ['$\\frac{t}{T}$', '$C_{D}$']]

results_1.show_lift_drag_general(data,
                               axis_labels,
                               show_plot=True,
                               save_figure=True,
                               figures_path=figures_path)

(a,b)=matplotlib.mlab.psd(array.remove_nan(results_1.lift_coefficient),
                          Fs=(1.0 / results_1.time_step),
                          detrend=None,
                          window=matplotlib.mlab.window_hanning,
                          pad_to=None,
                          sides=None,
                          scale_by_freq=True)

(a1,b1)=matplotlib.mlab.psd(array.remove_nan(cl_filtered),
                            Fs=(1.0 / results_1.time_step),
                            detrend=None,
                            window=matplotlib.mlab.window_hanning,
                            pad_to=None,
                            sides=None,
                            scale_by_freq=True)

plt.figure()
plt.plot(b, a, linestyle='-', label='LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018', color='green')
plt.plot(b1, a1, linestyle='-', label='LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018 - Filtered', color='blue')
plt.ylabel('PSD')
plt.xlabel('$f$ $(Hz)$')
plt.yscale("log")
plt.grid()
plt.legend()
plt.savefig(figures_path + "_psd.svg")

signal_obj = fft_tools.Signal(input_signal=array.remove_nan(cl_filtered),
                              sampling_frequency=(1.0 / results_1.time_step),
                              window='Hanning')

signal_obj1 = fft_tools.Signal(input_signal=array.remove_nan(results_1.lift_coefficient),
                              sampling_frequency=(1.0 / results_1.time_step),
                              window='Hanning')

plt.figure()
plt.semilogy(signal_obj1.freq_vect, signal_obj1.fft_signal_normalized, color='green', label='LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018')
plt.semilogy(signal_obj.freq_vect, signal_obj.fft_signal_normalized, color='blue', label='LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018 - Filtered')
plt.grid()
plt.xlabel('Frequency (Hz)')
plt.ylabel('Amplitude (-)')
plt.xlim([1.0, 25.0])
plt.legend()
plt.savefig(figures_path + "_fft.svg")

plt.show()





