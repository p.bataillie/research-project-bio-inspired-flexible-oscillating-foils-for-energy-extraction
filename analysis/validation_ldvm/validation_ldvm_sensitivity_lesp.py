import os
import numpy as np
from matplotlib import pyplot as plt
from utils import solver

os.chdir('../../')

case = "lift_64_deg_sym"

analysis_path = 'analysis/validation_ldvm/'
experimental_data_path = analysis_path + 'experimental_data/'
cases_path = analysis_path + 'cases/experimental.json'
data_exp = np.load(experimental_data_path + case + '/' + case + '.npy')
figures_path = analysis_path + 'figures/sensitivity_lesp/' + case

def func(time):
    time_arr = data_exp[:, 0]
    theta_arr = data_exp[:, 1]

    theta = np.radians(theta_arr[np.abs(time_arr - time).argmin()])
    h = 0.0

    return theta, h

solver_obj_1 = solver.Solver(filepath=cases_path, case_set="ldvm_sensitivity_0_05")
solver_obj_1.solve(func=func)
results_1 = solver_obj_1.output()

solver_obj_2 = solver.Solver(filepath=cases_path, case_set="ldvm_sensitivity_0_10")
solver_obj_2.solve(func=func)
results_2 = solver_obj_2.output()

solver_obj_3 = solver.Solver(filepath=cases_path, case_set="ldvm_sensitivity_0_15")
solver_obj_3.solve(func=func)
results_3 = solver_obj_3.output()

solver_obj_4 = solver.Solver(filepath=cases_path, case_set="ldvm_sensitivity_0_20")
solver_obj_4.solve(func=func)
results_4 = solver_obj_4.output()

solver_obj_5 = solver.Solver(filepath=cases_path, case_set="ldvm_sensitivity_0_25")
solver_obj_5.solve(func=func)
results_5 = solver_obj_5.output()

solver_obj_6 = solver.Solver(filepath=cases_path, case_set="ldvm_sensitivity_0_30")
solver_obj_6.solve(func=func)
results_6 = solver_obj_6.output()

# results.show_movement(show_plot=False)

period = results_1.model.time_manager.duration

plt.figure()
plt.plot(results_1.time / period, np.degrees(results_1.theta))
plt.xlabel('$\\frac{t}{T}$')
plt.ylabel('$\\theta$ ($^{\\circ}$)')
plt.grid()
plt.savefig(figures_path + "_motion.svg")

time_exp = np.linspace(data_exp[:, 0].min(), data_exp[:, 0].max(), 65)
cl_exp = np.interp(time_exp, data_exp[:, 0], data_exp[:, 2])

data = [[[results_1.time / period, results_1.lift_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018', 'grey'],
         [results_2.time / period, results_2.lift_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_2.critical_lesp) + ' - NACA0018', 'red'],
         [results_3.time / period, results_3.lift_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_3.critical_lesp) + ' - NACA0018', 'orange'],
         [results_4.time / period, results_4.lift_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_4.critical_lesp) + ' - NACA0018', 'green'],
         [results_5.time / period, results_5.lift_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_5.critical_lesp) + ' - NACA0018', 'blue'],
         [results_6.time / period, results_6.lift_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_6.critical_lesp) + ' - NACA0018', 'purple'],
         [time_exp / period, cl_exp, 'exp', 'S. Ōtomo and al. (2020) - NACA0018', 'black']],
        [[results_1.time / period, results_1.drag_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018', 'grey'],
         [results_2.time / period, results_2.drag_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_2.critical_lesp) + ' - NACA0018', 'red'],
         [results_3.time / period, results_3.drag_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_3.critical_lesp) + ' - NACA0018', 'orange'],
         [results_4.time / period, results_4.drag_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_4.critical_lesp) + ' - NACA0018', 'green'],
         [results_5.time / period, results_5.drag_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_5.critical_lesp) + ' - NACA0018', 'blue'],
         [results_6.time / period, results_6.drag_coefficient, 'num',
          'LDVM - $LESP = $' + str(results_6.critical_lesp) + ' - NACA0018', 'purple']]]

axis_labels = [['$\\frac{t}{T}$', '$C_{L}$'], ['$\\frac{t}{T}$', '$C_{D}$']]

results_1.show_lift_drag_general(data,
                                 axis_labels,
                                 show_plot=True,
                                 save_figure=True,
                                 figures_path=figures_path,
                                 legend_size=8)

# ou = np.zeros((np.size(results.time), 2))
# ou[:, 0] = results.time
# ou[:, 1] = results.lift_coefficient
# np.save(analysis_path + case + '_out.npy', ou)



