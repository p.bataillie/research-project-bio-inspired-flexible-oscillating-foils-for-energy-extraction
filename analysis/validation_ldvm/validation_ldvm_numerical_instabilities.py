import os
import numpy as np
from matplotlib import pyplot as plt
from utils import solver

os.chdir('../../')

case = "lift_64_deg_sym"

analysis_path = 'analysis/validation_ldvm/'
experimental_data_path = analysis_path + 'experimental_data/'
cases_path = analysis_path + 'cases/experimental.json'
data_exp = np.load(experimental_data_path + case + '/' + case + '.npy')
figures_path = analysis_path + 'figures/numerical_instabilities/' + case

def func(time):
    time_arr = data_exp[:, 0]
    theta_arr = data_exp[:, 1]

    theta = np.radians(theta_arr[np.abs(time_arr - time).argmin()])
    h = 0.0

    return theta, h

solver_obj_1 = solver.Solver(filepath=cases_path, case_set="ldvm_test_with_ldvm")
solver_obj_1.solve(func=func)
results_1 = solver_obj_1.output()

period = results_1.model.time_manager.duration

plt.figure()
plt.plot(results_1.time / period, np.degrees(results_1.theta))
plt.xlabel('$\\frac{t}{T}$')
plt.ylabel('$\\theta$ ($^{\\circ}$)')
plt.grid()
plt.savefig(figures_path + "_motion.svg")

time_exp = np.linspace(data_exp[:, 0].min(), data_exp[:, 0].max(), 65)
cl_exp = np.interp(time_exp, data_exp[:, 0], data_exp[:, 2])

data = [[[results_1.time / period, results_1.lift_coefficient, 'num', 'LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018', 'black']],
        [[results_1.time / period, results_1.drag_coefficient, 'num', 'LDVM - $LESP = $' + str(results_1.critical_lesp) + ' - NACA0018', 'black']]]

axis_labels = [['$\\frac{t}{T}$', '$C_{L}$'], ['$\\frac{t}{T}$', '$C_{D}$']]

results_1.show_lift_drag_general(data,
                               axis_labels,
                               show_plot=True,
                               save_figure=True,
                               figures_path=figures_path)

# ou = np.zeros((np.size(results.time), 2))
# ou[:, 0] = results.time
# ou[:, 1] = results.lift_coefficient
# np.save(analysis_path + case + '_out.npy', ou)



