import matplotlib.pyplot as plt
import numpy as np

original_file = "Lift_64asym"
output_file = "lift_64_deg_asym"

data = np.loadtxt(output_file + '/' + original_file + ".csv", delimiter=',', skiprows=1)

time_over_period = data[:, 0]
theta = data[:, 1]
lift = data[:, 2]

k = 0.22
u_inf = 0.215

b = 0.580
chord = 0.150

fp = (k * u_inf) / (np.pi * chord)
period = 1.0 / fp

data_save = np.zeros((np.size(time_over_period), 3))

data_save[:, 1] = theta

time = time_over_period * period
time_dimensionless = (time * u_inf) / chord

data_save[:, 0] = time

surface = chord * b
rho = 997

cl = lift / (0.5 * rho * (u_inf ** 2.0) * surface)

data_save[:, 2] = - cl

plt.figure()
plt.plot(data_save[:, 0], data_save[:, 1])
plt.grid()

plt.figure()
plt.plot(data_save[:, 0], data_save[:, 2])
plt.grid()

plt.show()

np.savetxt('experimental_data/' + output_file + '/' + output_file + '.csv', data_save, header="t/T, Theta [deg], Cl [N]")
np.save('experimental_data/' + output_file + '/' + output_file + '.npy', data_save)



















# period = 0.0
# time = np.linspace(0.0, period, 500)
# alpha = np.zeros(np.size(time))
# alpha_0 = 64.0
# xi = 0.5
# t_a = 0.15 * period
#
# alpha_1_dot = (2.0 * alpha_0) / (xi * period - t_a)
# alpha_2_dot = - (2.0 * alpha_0) / ((1.0 - xi) * period - t_a)
#
# for time_index in range(np.size(time)):
#     if 0.0 < time[time_index] < t1:
#         alpha[time_index] = alpha_1_dot * time[time_index]
#     elif t1 <= time[time_index] < t2:
#         alpha[time_index] = alpha_1_dot * time[time_index]