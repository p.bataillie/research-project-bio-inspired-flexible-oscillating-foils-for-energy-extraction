import numpy as np

import fft_tools

def remove_nan(arr):
    new_arr = arr[~np.isnan(arr)]
    return new_arr

data = np.load('data/lift_32_deg_asym_out.npy')
time = data[:, 0]
signal = data[:, 1]

signal = remove_nan(signal)
time = time[(np.size(time)-np.size(signal)):]

signal_obj = fft_tools.Signal(input_time=time, input_signal=signal,
                              window='Hanning')

# fft_tools.plot_time_signal(signal_obj.time, signal_obj.signal)
fft_tools.plot_fft(signal_obj.freq_vect, signal_obj.fft_signal_normalized)

# f = 0.1 Hz according to paper