import numpy as np
import scipy
from matplotlib import pyplot as plt


class Signal:
    def __init__(self, input_time=None, input_signal=None, sampling_frequency=None, zero_padding=None, block=None, window=None):
        if input_signal is None:
            raise Exception("Input Signal is None.")
        elif block is not None and zero_padding is not None:
            raise Exception("Block and Zero Padding applied at the same time.")

        if input_time is not None and input_signal is not None:
            self.input_time = input_time
            self.input_signal = input_signal
        elif sampling_frequency is not None and input_signal is not None:
            self.input_time = np.linspace(0, (np.size(input_signal) - 1) * 1 / sampling_frequency, np.size(input_signal))
            self.input_signal = input_signal

        self.time_sampling_criterion = True
        self.interp_value = 0.0

        self.check_interpolation()

        if self.time_sampling_criterion is True:
            self.time = self.input_time
            self.signal = self.input_signal
        elif self.time_sampling_criterion is False:
            print('INFO: Interpolation Required.')
            self.time = None
            self.signal = None
            self.interpolation()

        self.nb_samples = np.size(self.signal)  # Number of sample points
        self.ts = self.time[np.size(self.time) - 1] / (self.nb_samples - 1)  # Sample spacing

        if block is not None:
            # The blocking is applied here for an overlap of 50%.
            self.nb_block = block
            self.block_length = int(np.floor((2.0 / (self.nb_block + 1.0)) * np.size(self.signal)))
            self.block_indices_vect = np.zeros(self.nb_block + 2, dtype=np.int32)
            for i in range(np.size(self.block_indices_vect)):
                self.block_indices_vect[i] = int((self.block_length / 2.0) * i)
        elif block is None:
            self.nb_block = None

        if zero_padding is not None:
            self.zero_padding(zero_padding)
            self.nb_samples = np.size(self.signal)  # Number of sample points
            self.ts = self.time[np.size(self.time) - 1] / (self.nb_samples - 1)  # Sample spacing

        self.freq_vect = None
        self.fft_signal = None
        self.fft_signal_normalized = None
        self.window_normalization_factor = 1.0
        self.w = None

        if block is None:
            if window is not None:
                self.windowing(window=window)

            self.normalization_factor = (self.window_normalization_factor *
                                         2.0 *
                                         (1.0 / self.nb_samples) *
                                         (1 / np.sqrt(2.0)))

            self.fft()

            self.freq_vect = np.linspace(0.0, 1.0 / (2.0 * self.ts), int(self.nb_samples / 2))
        else:
            for i in range(0, self.nb_block):
                signal_block = self.signal[self.block_indices_vect[i]:self.block_indices_vect[i + 2]]

                if window is not None:
                    signal_block = self.windowing(window=window, signal_block=signal_block)

                self.normalization_factor = (self.window_normalization_factor *
                                             2.0 *
                                             (1.0 / self.block_length) *
                                             (1 / np.sqrt(2.0)))

                if self.fft_signal is None:
                    self.fft_signal = self.fft(signal_block=signal_block) / self.nb_block
                else:
                    self.fft_signal = self.fft_signal + self.fft(signal_block=signal_block) / self.nb_block

            self.fft_signal_normalized = self.normalization_factor * self.fft_signal
            self.freq_vect = np.linspace(0., 1.0 / (2.0 * self.ts), int(self.block_length / 2))

        self.spl = None

    def interpolation(self):
        self.time = np.linspace(self.input_time[0], self.input_time[np.size(self.input_time) - 1],
                                np.size(self.input_time) * 2)
        interp_obj = scipy.interpolate.interp1d(self.input_time, self.input_signal, kind='cubic')
        self.signal = interp_obj(self.time)

    def zero_padding(self, nb_zeros):
        # Number of sample points with padding
        nb_total = np.size(self.signal) + nb_zeros

        zeropadded_time = np.zeros(nb_total)
        zeropadded_signal = np.zeros(nb_total)

        zeropadded_time[:np.size(self.time)] = self.time
        zeropadded_time[np.size(self.time) - 1:] = np.linspace(self.time[np.size(self.time) - 1],
                                                           self.time[np.size(self.time) - 1] + nb_zeros * self.ts,
                                                           nb_zeros + 1)

        zeropadded_signal[:np.size(self.signal)] = self.signal

        self.time = zeropadded_time
        self.signal = zeropadded_signal

    def check_interpolation(self):
        diff = np.diff(self.input_time)
        diff_centered = diff - diff[0]
        nb_values = np.size(diff_centered)
        self.time_sampling_criterion = bool(np.abs(np.sum(diff_centered) / nb_values) < 1e-10)
        self.interp_value = np.size(np.array(np.where(np.abs(np.min(diff) - diff) < 1e-10)[0]))

    def windowing(self, window=None, amplitude=True, signal_block=None):
        if self.nb_block is None:
            if window == 'None':
                self.w = np.ones(self.nb_samples)
            elif window == 'Blackman':
                self.w = scipy.signal.windows.blackman(self.nb_samples)
            elif window == 'Hamming':
                self.w = scipy.signal.windows.hamming(self.nb_samples)
            elif window == 'Hanning':
                self.w = scipy.signal.windows.hann(self.nb_samples)

            if amplitude:
                # Amplitude conservation
                self.window_normalization_factor = 1 / np.mean(self.w)
            else:
                # Energy conservation
                self.window_normalization_factor = 1 / np.sqrt(np.mean(self.w ** 2))

            self.signal = self.signal * self.w
        else:
            if window == 'None':
                self.w = np.ones(self.block_length)
            elif window == 'Blackman':
                self.w = scipy.signal.windows.blackman(self.block_length)
            elif window == 'Hamming':
                self.w = scipy.signal.windows.hamming(self.block_length)
            elif window == 'Hanning':
                self.w = scipy.signal.windows.hann(self.block_length)

            if amplitude:
                # Amplitude conservation
                self.window_normalization_factor = 1 / np.mean(self.w)
            else:
                # Energy conservation
                self.window_normalization_factor = 1 / np.sqrt(np.mean(self.w ** 2))

            return signal_block * self.w

    def fft(self, signal_block=None):
        if self.nb_block is None:
            fft_signal = scipy.fftpack.fft(self.signal)
            self.fft_signal = np.abs(fft_signal[:self.nb_samples // 2])
            self.fft_signal_normalized = self.normalization_factor * self.fft_signal
        else:
            fft_signal = scipy.fftpack.fft(signal_block)
            fft_signal = np.abs(fft_signal[:self.block_length // 2])
            return fft_signal

    def sound_pressure_level(self, show_plot=False, save_figure=None):
        # Reference pressure
        pref = 2e-05

        self.spl = 20 * np.log10(self.fft_signal_normalized / pref)

        plt.figure(3, figsize=(20, 5))
        plt.plot(self.freq_vect, self.spl, 'k-o')
        plt.grid()
        plt.xlabel('Frequency (Hz)')
        plt.ylabel('Amplitude (dB)')
        plt.xscale('log')

        if save_figure is not None:
            plt.savefig('figures/' + save_figure + '.svg')

        if show_plot is True:
            plt.show()


def plot_time_signal(time, signal):
    # Plot the signal
    plt.figure(1, figsize=(20, 5))
    plt.plot(time, signal, 'k-')
    plt.grid()
    plt.xlabel('Time (s)')
    plt.ylabel('Signal S (-)')
    plt.show()


def plot_phase_portrait(time, signal):
    # Plot the phase portrait
    # compute dS/dt
    dS = np.diff(signal)
    dt = np.mean(np.diff(time))
    dS = np.append(dS[0], dS) # get the same length as the signal
    # compute s(t-tau)
    plt.figure(2, figsize=(10, 10))
    plt.plot(signal, dS/dt, 'k-')
    plt.grid()
    plt.xlabel('Signal S (-)')
    plt.ylabel('dS/dt (-)')
    plt.show()


def plot_fft(freq, fft_signal_normalized, y_lim=None):
    plt.figure()
    plt.loglog(freq, fft_signal_normalized, 'k-o')
    plt.grid()
    plt.xlabel('Frequency (Hz)')
    plt.ylabel('Amplitude (-)')
    if y_lim is not None:
        plt.ylim(y_lim)
    plt.show()
