import os
import torch
from mpi4py import MPI

comm = MPI.COMM_WORLD
rank = comm.Get_rank()
mpi_size = comm.Get_size()
procname = MPI.Get_processor_name()

if rank == 0:
    print(os.getcwd())

from utils import solver
import numpy as np


def kinematics(vars):
    pitching_amplitude = vars[0]
    heaving_amplitude = vars[1]

    def func(time):
        frequency = 1.0
        angular_pulsation = 2.0 * np.pi * frequency

        theta = np.radians(pitching_amplitude) * np.cos(angular_pulsation * time)
        h = heaving_amplitude * np.sin(angular_pulsation * time)

        return theta, h
    return func


def generate_array_cases(range_variables):
    temp = []
    nb_variables = np.size(range_variables, 0)
    for i in range(nb_variables):
        temp.append(np.linspace(range_variables[i][0], range_variables[i][1], int(range_variables[i][2])))

    cases = []

    for k in range(np.size(temp[0])):
        var_1 = temp[0][k]
        for j in range(np.size(temp[1])):
            var_2 = temp[1][j]
            cases.append(np.array([var_1, var_2]))

    return np.array(cases)


def iterations(cases, dataset_inputs, dataset_outputs, rank):
    k = 0
    for i in range(np.size(cases, 0)):
        print('Process ' + str(rank) + ': ' + str(i + 1) + '/' + str(np.size(cases, 0)))
        func = kinematics(cases[i])
        solver_obj = solver.Solver(filepath="mlp/dataset/cases/dataset.json", case_set="ldvm")
        solver_obj.solve(func=func)
        results = solver_obj.output()

        time = results.time[results.time >= 1.0] - 1.0
        theta = results.theta[results.time >= 1.0]
        theta_dot = results.theta_dot[results.time >= 1.0]
        theta_dot_2 = results.theta_dot_2[results.time >= 1.0]
        h = results.h[results.time >= 1.0]
        h_dot = results.h_dot[results.time >= 1.0]
        h_dot_2 = results.h_dot_2[results.time >= 1.0]
        lift_coefficient = results.lift_coefficient[results.time >= 1.0]

        time_step = results.time_step
        inputs = []
        outputs = []

        for i in range(18, np.size(time)):
            res_1 = np.array([theta[i], theta_dot[i], h[i], h_dot[i]])
            res_2 = np.array([theta[i - 9], theta_dot[i - 9], h[i - 9], h_dot[i - 9]])
            res_3 = np.array([theta[i - 18], theta_dot[i - 18], h[i - 18], h_dot[i - 18]])
            inputs_temp = np.array([res_1, res_2, res_3]).flatten()
            outputs_temp = lift_coefficient[i]
            if not np.isnan(outputs_temp) and np.abs(outputs_temp) < 100.0:
                inputs.append(inputs_temp)
                outputs.append(outputs_temp)
            else:
                inputs.append(np.zeros((1, 12)))
                outputs.append(0.0)

        inputs = np.array(inputs, dtype=object)
        outputs = np.array(outputs, dtype=object)

        for nb in range(len(inputs)):
            dataset_inputs[k + nb] = inputs[nb]
            dataset_outputs[k + nb] = outputs[nb]

        k = k + len(inputs)

    return dataset_inputs, dataset_outputs


variables = ["theta_0", "h_0"]
range_variables = np.array([[0.0, 45.0, 20], [0.05, 0.22, 20]])

# solver_obj = solver.Solver(filepath="mlp/dataset/cases/experimental.json", case_set="ldvm")

if rank == 0:
    cases_array_temp = generate_array_cases(range_variables)
    cases_array = np.array_split(cases_array_temp, mpi_size)
    cases_array = np.array(cases_array, dtype=object)

    nb_time_step = int(((501 - 1) / 2) - 18 + 1)

    dataset_inputs_temp = []
    dataset_outputs_temp = []
    for k in range(np.size(cases_array, 0)):
        dataset_inputs_temp.append(np.zeros((nb_time_step * np.size(cases_array[k], 0), 12)))
        dataset_outputs_temp.append(np.zeros((nb_time_step * np.size(cases_array[k], 0), 1)))
    dataset_inputs = np.array(dataset_inputs_temp, dtype=object)
    dataset_outputs = np.array(dataset_outputs_temp, dtype=object)

else:
    cases_array = None
    dataset_inputs = None
    dataset_outputs = None

cases_array = comm.scatter(cases_array, root=0)
dataset_inputs = comm.scatter(dataset_inputs, root=0)
dataset_outputs = comm.scatter(dataset_outputs, root=0)

print(str(rank) + ': ' + str(np.size(dataset_inputs, 0)))

dataset_inputs, dataset_outputs = iterations(cases_array, dataset_inputs, dataset_outputs, rank)

cases_array = comm.gather(cases_array, root=0)
dataset_inputs = comm.gather(dataset_inputs, root=0)
dataset_outputs = comm.gather(dataset_outputs, root=0)

if rank == 0:
    dataset_inputs = np.array(np.concatenate(dataset_inputs), dtype=float)
    dataset_outputs = np.array(np.concatenate(dataset_outputs), dtype=float)

    torch.save(torch.tensor(dataset_outputs), 'mlp/dataset/outputs/dataset_outputs.pt')
    torch.save(torch.tensor(dataset_inputs), 'mlp/dataset/outputs/dataset_inputs.pt')



    # plt.figure()
    # plt.plot(time, h)
    # plt.plot(time, h_dot)
    # plt.plot(time, h_dot_2)
    #
    # plt.figure()
    # plt.plot(time, np.degrees(theta))
    # plt.plot(time, theta_dot)
    # plt.plot(time, theta_dot_2)
    # plt.plot(time, - np.radians(10.0) * (2.0 * np.pi * 1.0) * np.sin(2.0 * np.pi * 1.0 * time))
    # plt.plot(time, - np.radians(10.0) * ((2.0 * np.pi * 1.0) ** 2) * np.cos(2.0 * np.pi * 1.0 * time))
    #
    # plt.figure()
    # plt.plot(results.time, results.lift_coefficient)
    #
    # plt.show()



