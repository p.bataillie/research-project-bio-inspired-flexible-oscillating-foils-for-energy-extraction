import os
import subprocess
import tempfile
import shutil
import numpy as np
from matplotlib import pyplot as plt
import time


class LDVM:
    def __init__(self, project_dir, ldvm_dir):
        self.project_dir = project_dir
        self.ldvm_dir = self.project_dir + ldvm_dir
        self.name_source_code = 'ldvm.f95'

        self.temp = tempfile.TemporaryDirectory()
        self.temp_dir = self.temp.name

        self.name_airfoil_file = None

        shutil.copy2(self.ldvm_dir + self.name_source_code, self.temp_dir)

        os.chdir(self.temp_dir)

        self.out = None

        self.name_kinematics_file = "input_kinematics.dat"
        self.name_data_output_file = "output_data.dat"
        self.name_visu_output_file = "output_visu.dat"
        self.name_input_file = "input_LDVM.dat"

        self.output_data = None
        self.output_visualisation = None


    def run(self, show_output=False, save_visualisation=False):
        if not os.path.isfile(self.name_source_code):
            raise FileNotFoundError

        if not os.path.isfile(self.name_airfoil_file):
            raise FileNotFoundError

        if not os.path.isfile(self.name_input_file):
            raise FileNotFoundError

        if not os.path.isfile(self.name_kinematics_file):
            raise FileNotFoundError

        subprocess.run(["gfortran ldvm.f95 -o ldvm"], shell=True, capture_output=True, text=True)
        # self.out = subprocess.run(["./ldvm"], shell=True, capture_output=True, text=True)

        # invoke process
        process = subprocess.Popen("./ldvm", shell=False, stdout=subprocess.PIPE)

        if show_output:
            while True:
                output = process.stdout.readline()
                if process.poll() is not None:
                    break
                if output:
                    print(str(output.strip().decode('UTF-8')))
        # else:
        #     animation = [
        #         "[        ]",
        #         "[=       ]",
        #         "[===     ]",
        #         "[====    ]",
        #         "[=====   ]",
        #         "[======  ]",
        #         "[======= ]",
        #         "[========]",
        #         "[ =======]",
        #         "[  ======]",
        #         "[   =====]",
        #         "[    ====]",
        #         "[     ===]",
        #         "[      ==]",
        #         "[       =]",
        #         "[        ]",
        #         "[        ]"
        #     ]
        #
        #     i = 0
        #
        #     while True:
        #         output = process.stdout.readline()
        #         if process.poll() is not None:
        #             break
        #         if output:
        #             print(animation[i % len(animation)])
        #             time.sleep(.1)
        #             i += 1
        #             if os.name == 'nt':
        #                 os.system('cls')
        #             else:
        #                 os.system('clear')

        process.wait()
        self.postprocessing(save_visualisation=save_visualisation)

        self.temp.cleanup()
        os.chdir(self.project_dir)

    def setup_kinematics(self, non_dimensional_time, theta_in_degrees, h_c):
        u = np.ones(np.size(non_dimensional_time))

        export = np.zeros((len(non_dimensional_time), 4), dtype=np.float32)
        export[:, 0] = np.round(non_dimensional_time, 8)
        export[:, 1] = np.round(theta_in_degrees, 8)
        export[:, 2] = np.round(h_c, 8)
        export[:, 3] = np.round(u, 8)

        export = np.round(export, 8)
        np.savetxt(self.name_kinematics_file, export, delimiter="   ", fmt='%.7e', newline="\n   ")

    def setup_inputs(self, chord, freestream_velocity, pitching_axis_position, airfoil_name, reynolds_number, critical_lesp, output_frequency):
        f = open(self.name_input_file, "w", encoding="utf-8")
        f.write(str(chord) + "\n")
        f.write(str(freestream_velocity) + "\n")
        f.write(str(pitching_axis_position) + "\n")
        f.write(str(pitching_axis_position) + "\n")
        self.name_airfoil_file = airfoil_name + ".dat"
        f.write(self.name_airfoil_file + "\n")
        shutil.copy2(self.ldvm_dir + self.name_airfoil_file, self.temp_dir)
        f.write(str(reynolds_number) + "\n")
        f.write(str(critical_lesp) + "\n")
        f.write(self.name_kinematics_file + "\n")
        f.write(self.name_data_output_file + "\n")
        f.write(self.name_visu_output_file + "				" + str(output_frequency))
        f.close()

    def postprocessing(self, save_visualisation=False):
        self.output_data = np.loadtxt(self.name_data_output_file)
        if save_visualisation:
            self.output_visualisation = np.loadtxt(self.name_visu_output_file)
            
    def visualisation(self):
        if self.output_visualisation is not None:
            start_ind = np.where(np.isnan(self.output_visualisation[:, 0]))[0]
            n_steps = len(start_ind) - 1

            fig, ax = plt.subplots(1, 1, figsize=(20, 5))

            sc = ax.scatter([], [], c=[], s=1.5, cmap='jet', vmin=-0.01, vmax=0.01)
            ax.set_aspect('equal')
            ax.set_xlim([-15, 1.2])
            ax.set_ylim([-1.0, 1.0])
            # plt.colorbar(sc, ax=ax)

            for step in range(n_steps):
                vort = self.output_visualisation[start_ind[step] + 1:start_ind[step + 1], :]

                sc.set_offsets(vort[:, 1:3])
                sc.set_array(vort[:, 0])

                plt.pause(0.001)

            plt.show()
        else:
            raise Exception("Error: Visualisation Data not saved!")
