import numpy as np
import model_greenberg
import math


class GranlundModel(model_greenberg.GreenbergModel):
    def __init__(self, granlund_input_data, time_manager):
        super().__init__(granlund_input_data, time_manager)

        self.drag_coefficient_added_mass = np.zeros(self.time_size, dtype=complex)
        self.drag_coefficient_buoyancy = np.zeros(self.time_size, dtype=complex)

    def drag(self, time_index):
        self.drag_coefficient_added_mass[time_index] = (self.alpha_eff[time_index] ** 2) * math.pi * self.surge_amplitude * self.k * np.exp(1j * self.w * self.time[time_index])
        self.drag_coefficient_buoyancy[time_index] = 4.0
