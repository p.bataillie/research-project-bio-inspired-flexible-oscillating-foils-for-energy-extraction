import math
import numpy as np
import scipy

from models import model_theodorsen


class GarrickModel(model_theodorsen.TheodorsenModel):
    def __init__(self, garrick_input_data):
        super().__init__(garrick_input_data)

        self.B1 = None
        self.B2 = None
        self.B3 = None
        self.B4 = None
        self.B5 = None
        self.B6 = None

        self.C1 = None
        self.C2 = None
        self.C3 = None
        self.C4 = None
        self.C5 = None
        self.C6 = None

        self.A1 = None
        self.A2 = None
        self.A3 = None
        self.A4 = None
        self.A5 = None
        self.A6 = None

        self.a1 = None
        self.a2 = None
        self.a3 = None
        self.a4 = None
        self.a5 = None
        self.a6 = None

        self.b2 = None
        self.b4 = None
        self.b6 = None

        self.c3 = None
        self.c5 = None
        self.c6 = None

        self.D = (((scipy.special.jv(1, self.k) + scipy.special.yv(0, self.k)) ** 2)
                  + ((scipy.special.yv(1, self.k) - scipy.special.jv(0, self.k)) ** 2))
        self.J = (scipy.special.jv(1, self.k) + scipy.special.yv(0, self.k)) / self.D
        self.K = (scipy.special.yv(1, self.k) - scipy.special.jv(0, self.k)) / self.D

        self.S = np.zeros(self.time_size, dtype=complex)
        self.S_real = np.zeros(self.time_size)
        self.px = np.zeros(self.time_size, dtype=complex)
        self.px_real = np.zeros(self.time_size)

        self.time_averaged_drag = None
        self.time_averaged_drag_coefficients = None
        self.time_averaged_drag_coefficients_2nd = None

        self.time_averaged_propulsive_force = None
        self.time_averaged_propulsive_force_coefficients = None
        self.time_averaged_propulsive_force_coefficients_2nd = None

        self.M_real = None
        self.N_real = None

        self.time_averaged_total_work = None
        self.time_averaged_total_work_coefficients = None
        self.time_averaged_ke_coefficients = None
        self.time_averaged_ke = None

        self.efficiency_garrick = None
        self.efficiency_garrick_coefficients = None
        self.efficiency_garrick_coefficients_2nd = None

        self.compute_garrick_coefficients()

    def compute_forces(self, time_index):
        super().compute_forces(time_index)
        self.s_component_real(time_index)
        self.s_component(time_index)
        self.drag(time_index)

    def compute_time_averaged_quantities(self):
        self.compute_time_averaged_propulsive_force()
        self.compute_time_averaged_drag()
        self.compute_time_averaged_total_work()
        self.compute_time_averaged_ke()

    def compute_efficiency(self):
        self.compute_garrick_efficiency()

    def s_component(self, time_index):
        self.S[time_index] = (np.sqrt(2.0) / 2.0) * (2.0 * self.C * self.Q[time_index]
                                                     - self.b * self.theta_dot[time_index]
                                                     - (2.0 / math.pi) * np.sqrt(1 - (self.e ** 2)) * self.u_inf * self.delta[time_index]
                                                     + (self.t4 / math.pi) * self.b * self.delta_dot[time_index])

        self.px[time_index] = (math.pi * self.rho * (self.S[time_index] ** 2)
                               + (self.theta[time_index] * self.lift_force_airfoil_complex[time_index])
                               + (self.delta[time_index] * self.lift_force_aileron_complex[time_index]))

    def s_component_real(self, time_index):
        self.S_real[time_index] = (np.sqrt(2.0) / 2.0) * (self.M_real * np.sin(self.w * self.time[time_index])
                                                          + self.N_real * np.cos(self.w * self.time[time_index]))

    def drag(self, time_index):
        self.propulsive_force_airfoil[time_index] = self.theta[time_index].imag * self.lift_force_airfoil_complex[time_index].imag
        self.propulsive_force_aileron[time_index] = self.delta[time_index].imag * self.lift_force_aileron_complex[time_index].imag

        self.propulsive_force_total[time_index] = (math.pi * self.rho * (self.S[time_index].imag ** 2)
                                                   + self.propulsive_force_airfoil[time_index]
                                                   + self.propulsive_force_aileron[time_index])

        self.propulsive_coefficient[time_index] = self.propulsive_force_total[time_index] / (self.dynamic_pressure * self.c)

        self.drag_force_airfoil[time_index] = - self.propulsive_force_airfoil[time_index]
        self.drag_force_aileron[time_index] = - self.propulsive_force_aileron[time_index]
        self.drag_force_total[time_index] = - self.propulsive_force_total[time_index]
        self.drag_coefficient[time_index] = - self.propulsive_coefficient[time_index]

    def compute_garrick_coefficients(self):
        self.M_real = (2.0 * self.F * (self.u_inf * self.theta_0 * np.cos(self.phi_0)
                                                   - self.h_0 * self.w * np.sin(self.phi_2)
                                                   - self.b * ((1 / 2) - self.a) * self.theta_0 * self.w * np.sin(self.phi_0)
                                                   + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.cos(self.phi_1)
                                                   - (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.sin(self.phi_1))
                                   - 2.0 * self.G * (self.u_inf * self.theta_0 * np.sin(self.phi_0)
                                                     + self.h_0 * self.w * np.cos(self.phi_2)
                                                     + self.b * ((1 / 2) - self.a) * self.theta_0 * self.w * np.cos(self.phi_0)
                                                     + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.sin(self.phi_1)
                                                     + (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.cos(self.phi_1))
                                   + self.b * self.theta_0 * self.w * np.sin(self.phi_0)
                                   - (2 / math.pi) * np.sqrt(1 - (self.e ** 2)) * self.u_inf * self.delta_0 * np.cos(self.phi_1)
                                   - (self.t4 / math.pi) * self.b * self.delta_0 * self.w * np.sin(self.phi_1))

        self.N_real = (2.0 * self.F * (self.u_inf * self.theta_0 * np.sin(self.phi_0)
                                                   + self.h_0 * self.w * np.cos(self.phi_2)
                                                   + self.b * ((1 / 2) - self.a) * self.theta_0 * self.w * np.cos(self.phi_0)
                                                   + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.sin(self.phi_1)
                                                   + (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.cos(self.phi_1))
                                   + 2.0 * self.G * (self.u_inf * self.theta_0 * np.cos(self.phi_0)
                                                     - self.h_0 * self.w * np.sin(self.phi_2)
                                                     - self.b * ((1 / 2) - self.a) * self.theta_0 * self.w * np.sin(self.phi_0)
                                                     + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.cos(self.phi_1)
                                                     - (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.sin(self.phi_1))
                                   - self.b * self.theta_0 * self.w * np.cos(self.phi_0)
                                   - (2 / math.pi) * np.sqrt(1 - (self.e ** 2)) * self.u_inf * self.delta_0 * np.sin(self.phi_1)
                                   + (self.t4 / math.pi) * self.b * self.delta_0 * self.w * np.cos(self.phi_1))

        self.B1 = self.F
        self.B2 = (self.b ** 2) * ((1 / 2) * (1 / 2 - self.a) - (self.a + 1 / 2) * (self.F * (1 / 2 - self.a) + (self.G / self.k)))
        self.B3 = (self.b ** 2) * (- (self.t19 / (4.0 * (math.pi ** 2)))
                                   + (self.t12 / (2.0 * math.pi)) * ((self.t11 / (2.0 * math.pi)) * self.F + (self.t10 / math.pi) * (self.G / self.k)))
        self.B4 = (self.b / 2.0) * (((1 / 2) - 2.0 * self.a * self.F + (self.G / self.k)) * np.cos(self.phi_2 - self.phi_0)
                                    - ((self.F / self.k) - self.G) * np.sin(self.phi_2 - self.phi_0))
        self.B5 = (self.b / 2.0) * ((- self.t4 / (2.0 * math.pi) + ((self.t11 + self.t12) / (2.0 * math.pi)) * self.F + (self.t10 / math.pi) * (self.G / self.k)) * np.cos(self.phi_2 - self.phi_1)
                                    - ((self.t10 / math.pi) * (self.F / self.k) + (self.t4 / math.pi) * self.G) * np.sin(self.phi_2 - self.phi_1))
        self.B6 = ((self.b ** 2) / 2.0) * ((self.t11 / (4.0 * math.pi)
                                            - (1 / 2 - self.a) * (self.t4 / (2.0 * math.pi))
                                            + ((self.t4 / (2.0 * math.pi)) - ((self.t11 + self.t12) / (2.0 * math.pi)) * self.a) * self.F
                                            - (((self.a + 1 / 2) * (self.t10 / math.pi) - (self.t12 / (2.0 * math.pi))) * (self.G / self.k))) * np.cos(self.phi_1 - self.phi_0)
                                           + (((self.t15 / (2.0 * math.pi)) * (1.0 / self.k))
                                                - ((self.a + 1 / 2) * (self.t10 / math.pi) + (self.t12 / (2.0 * math.pi))) * (self.F / self.k)
                                                + ((self.t11 + self.t12) / (4.0 * math.pi) - (self.t4 * self.a) / math.pi) * self.G) * np.sin(self.phi_1 - self.phi_0))

        self.C1 = 2.0 / (math.pi * self.k * self.D)
        self.C2 = ((2.0 * (self.b ** 2)) / (math.pi * self.k * self.D)) * ((1.0 / (self.k ** 2)) + ((0.5 - self.a) ** 2))

        self.C3 = ((2.0 * (self.b ** 2)) / (math.pi * self.k * self.D)) * (((self.t10 / (math.pi * self.k)) ** 2)
                                                                           + ((self.t11 / (2.0 * math.pi)) ** 2))

        self.C4 = ((2.0 * self.b) / (math.pi * self.k * self.D)) * (- (1.0 / self.k) * math.sin(self.phi_2 - self.phi_0)
                                                                    + (1 / 2 - self.a) * math.cos(self.phi_2 - self.phi_0))

        self.C5 = ((2.0 * self.b) / (math.pi * self.k * self.D)) * (- (self.t10 / (math.pi * self.k)) * math.sin(self.phi_2 - self.phi_1)
                                                                    + (self.t11 / (2 * math.pi)) * math.cos(self.phi_2 - self.phi_1))

        self.C6 = ((2.0 * (self.b ** 2)) / (math.pi * self.k * self.D)) * (((self.t10 / (math.pi * (self.k ** 2))) + (1 / 2 - self.a) * (self.t11 / (2.0 * math.pi))) * math.cos(self.phi_1 - self.phi_0)
                                                                           + (1.0 / self.k) * ((self.t10 / math.pi) * (1 / 2 - self.a) - (self.t11 / (2.0 * math.pi))) * math.sin(self.phi_1 - self.phi_0))

        self.A1 = self.B1 - self.C1
        self.A2 = self.B2 - self.C2
        self.A3 = self.B3 - self.C3
        self.A4 = self.B4 - self.C4
        self.A5 = self.B5 - self.C5
        self.A6 = self.B6 - self.C6

        self.a1 = self.F ** 2 + self.G ** 2
        self.a2 = self.b ** 2 * ((self.F ** 2 + self.G ** 2) * (1 / self.k ** 2 + (0.5 - self.a) ** 2) + 0.25 - (
                0.5 - self.a) * self.F - 1 / self.k * self.G)
        self.a3 = (self.b ** 2) * (((self.F ** 2) + (self.G ** 2)) * ((self.t10 / (math.pi * self.k)) ** 2
                                                                + (self.t11 / (math.pi * 2)) ** 2)
                                 + (1 - self.e ** 2) / ((math.pi ** 2) * (self.k ** 2))
                                 + (self.t4 ** 2) / (4 * (math.pi ** 2))
                                 + self.F * ((-2 * self.t10 * np.sqrt(1 - self.e ** 2)) / ((math.pi ** 2) * (self.k ** 2))
                                             + (self.t4 * self.t11) / (2 * (math.pi ** 2)))
                                 + (self.G / self.k) * ((self.t4 * self.t10) / (math.pi ** 2)
                                                      + (self.t11 * np.sqrt(1 - self.e ** 2)) / (math.pi ** 2)))
        self.a4 = self.b * (((self.F ** 2) + (self.G ** 2)) * (- (1.0 / self.k) * math.sin(self.phi_2 - self.phi_0)
                                                               + (0.5 - self.a) * math.cos(self.phi_2 - self.phi_0))
                            - (self.F / 2.0) * math.cos(self.phi_2 - self.phi_0)
                            + (self.G / 2.0) * math.sin(self.phi_2 - self.phi_0))
        self.a5 = self.b * ((self.F ** 2 + self.G ** 2) * (-self.t10 / (math.pi * self.k) * math.sin(self.phi_2 - self.phi_1)
                                                           + self.t11 / (2 * math.pi) * math.cos(self.phi_2 - self.phi_1))
                            + self.F / 2 * (2 * np.sqrt(1 - self.e ** 2) / (math.pi * self.k) * math.sin(self.phi_2 - self.phi_1)
                                            + self.t4 / math.pi * math.cos(self.phi_2 - self.phi_1))
                            + self.G / 2 * (2 * np.sqrt(1 - self.e ** 2) / (math.pi * self.k) * math.cos(self.phi_2 - self.phi_1)
                                            - self.t4 / math.pi * math.sin(self.phi_2 - self.phi_1)))
        self.a6 = (self.b ** 2) * (((self.F ** 2) + (self.G ** 2)) * ((self.t10 / (math.pi * (self.k ** 2)) + (self.t11 / (2.0 * math.pi)) * (0.5 - self.a)) * math.cos(self.phi_1 - self.phi_0)
                                                                      + ((self.t10 / math.pi) * (0.5 - self.a) - (self.t11 / (2.0 * math.pi))) * (1.0 / self.k) * math.sin(self.phi_1 - self.phi_0))
                                   + (self.F / 2.0) * ((- 2.0 * np.sqrt(1 - (self.e ** 2))) / (math.pi * (self.k ** 2))
                                                       + (self.t4 / math.pi) * (0.5 - self.a)
                                                       - (self.t11 / (2.0 * math.pi))) * math.cos(self.phi_1 - self.phi_0)
                                   - (((2.0 * np.sqrt(1 - (self.e ** 2))) / math.pi) * (0.5 - self.a)
                                      + ((self.t4 + self.t10) / math.pi)) * (1.0 / self.k) * math.sin(self.phi_1 - self.phi_0)
                                   + (self.G / 2.0) * (((- 2.0 * np.sqrt(1 - self.e ** 2)) / (math.pi * (self.k ** 2)))
                                                       + (self.t4 / math.pi) * (0.5 - self.a)
                                                       + (self.t11 / (2.0 * math.pi))) * math.sin(self.phi_1 - self.phi_0)
                                   + (((2.0 * np.sqrt(1 - self.e ** 2)) / math.pi) * (0.5 - self.a)
                                      + ((self.t4 - self.t10) / math.pi)) * (1.0 / self.k) * math.cos(self.phi_1 - self.phi_0)
                                   + (np.sqrt(1.0 - (self.e ** 2)) / (2.0 * math.pi)) * (1.0 / self.k) * math.sin(self.phi_1 - self.phi_0)
                                   - (self.t4 / (4.0 * math.pi)) * math.cos(self.phi_1 - self.phi_0))

        self.b2 = self.b ** 2 * (-self.a / 2 - self.F / self.k ** 2 + (0.5 - self.a) * self.G / self.k)
        self.b4 = (self.b / 2.0) * ((0.5 + (self.G / self.k)) * math.cos(self.phi_2 - self.phi_0)
                                    + (self.F / self.k) * math.sin(self.phi_2 - self.phi_0))
        self.b6 = ((self.b ** 2) / 2) * ((- (self.t1 / (2 * math.pi))
                                          - (self.F / (self.k ** 2)) * (self.t10 / math.pi)
                                          + (self.G / self.k) * (self.t11 / (2.0 * math.pi))) * math.cos(self.phi_1 - self.phi_0)
                                         + (- (self.t4 / (2.0 * math.pi))
                                            + self.F * (self.t11 / (2.0 * math.pi))
                                            + (self.G / self.k) * (self.t10 / math.pi)) * (1 / self.k) * math.sin(self.phi_1 - self.phi_0))

        self.c3 = (self.b ** 2) * (- self.t2 / (2.0 * (math.pi ** 2))
                                 - (1 - (self.e ** 2)) / ((math.pi ** 2) * (self.k ** 2))
                                 - (self.F / self.k ** 2) * ((self.t10 * self.t20) / (math.pi ** 2))
                                 + (self.G / self.k) * ((self.t11 * self.t20) / (2.0 * (math.pi ** 2))))
        self.c5 = self.b / 2 * ((-self.t4 / 2 / math.pi + self.t20 / math.pi * self.G / self.k) * math.cos(self.phi_2 - self.phi_1)
                                + self.t20 / math.pi * self.F / self.k * math.sin(self.phi_2 - self.phi_1))
        self.c6 = ((self.b ** 2) / 2) * (((self.t9 / math.pi)
                                          - (self.t20 / math.pi) * (self.F / (self.k ** 2))
                                          + (self.t20 / math.pi) * (0.5 - self.a) * (self.G / self.k)) * math.cos(self.phi_1 - self.phi_0)
                                         + (((self.t4 - (1 - self.e) * np.sqrt(1 - (self.e ** 2))) / (2.0 * math.pi))
                                            - (self.t20 / math.pi) * (0.5 - self.a) * self.F
                                            - (self.t20 / math.pi) * (self.G / self.k)) * (1 / self.k) * math.sin(self.phi_1 - self.phi_0))

    def compute_time_averaged_propulsive_force(self):
        self.time_averaged_propulsive_force = (self.w / (2.0 * math.pi)) * np.trapz(self.propulsive_force_total, x=self.time)

        self.time_averaged_propulsive_force_coefficients = math.pi * self.rho * self.b * (self.w ** 2) * (self.A1 * (self.h_0 ** 2)
                                                                                                          + self.A2 * (self.theta_0 ** 2)
                                                                                                          + self.A3 * (self.delta_0 ** 2)
                                                                                                          + 2.0 * self.A4 * self.theta_0 * self.h_0
                                                                                                          + 2.0 * self.A5 * self.delta_0 * self.h_0
                                                                                                          + 2.0 * self.A6 * self.theta_0 * self.delta_0)

        self.time_averaged_propulsive_force_coefficients_2nd = math.pi * self.rho * self.b * (self.w ** 2) * (self.a1 * (self.h_0 ** 2)
                                                                                                              + (self.a2 + self.b2) * (self.theta_0 ** 2)
                                                                                                              + (self.a3 + self.c3) * (self.delta_0 ** 2)
                                                                                                              + 2.0 * (self.a4 + self.b4) * self.theta_0 * self.h_0
                                                                                                              + 2.0 * (self.a5 + self.c5) * self.delta_0 * self.h_0
                                                                                                              + 2.0 * (self.a6 + self.b6 + self.c6) * self.theta_0 * self.delta_0)

    def compute_time_averaged_drag(self):
        self.time_averaged_drag = (self.w / (2.0 * math.pi)) * np.trapz(self.drag_force_total, x=self.time)
        self.time_averaged_drag_coefficients = - self.time_averaged_propulsive_force_coefficients
        self.time_averaged_drag_coefficients_2nd = - self.time_averaged_propulsive_force_coefficients_2nd

    def compute_time_averaged_total_work(self):
        self.time_averaged_total_work = - (self.w / (2.0 * math.pi)) * np.trapz(self.lift_force_airfoil * self.h_dot.imag
                                                                                + self.moment_airfoil * self.theta_dot.imag
                                                                                + self.moment_aileron * self.delta_dot.imag,
                                                                                x=self.time)

        self.time_averaged_total_work_coefficients = math.pi * self.rho * (self.b ** 2) * ((self.w ** 3) / self.k) * (self.B1 * (self.h_0 ** 2)
                                                                                                                      + self.B2 * (self.theta_0 ** 2)
                                                                                                                      + self.B3 * (self.delta_0 ** 2)
                                                                                                                      + 2.0 * self.B4 * self.theta_0 * self.h_0
                                                                                                                      + 2.0 * self.B5 * self.delta_0 * self.h_0
                                                                                                                      + 2.0 * self.B6 * self.theta_0 * self.delta_0)

    def compute_time_averaged_ke(self):
        self.time_averaged_ke_coefficients = math.pi * self.rho * (self.b ** 2) * ((self.w ** 3) / self.k) * (self.C1 * (self.h_0 ** 2)
                                                                                                              + self.C2 * (self.theta_0 ** 2)
                                                                                                              + self.C3 * (self.delta_0 ** 2)
                                                                                                              + 2.0 * self.C4 * self.theta_0 * self.h_0
                                                                                                              + 2.0 * self.C5 * self.delta_0 * self.h_0
                                                                                                              + 2.0 * self.C6 * self.theta_0 * self.delta_0)

    def compute_garrick_efficiency(self):
        if self.time_averaged_total_work != 0.0:
            self.efficiency_garrick = (self.time_averaged_propulsive_force * self.u_inf) / self.time_averaged_total_work

        if self.time_averaged_total_work_coefficients != 0.0:
            self.efficiency_garrick_coefficients = (self.time_averaged_propulsive_force_coefficients * self.u_inf) / self.time_averaged_total_work_coefficients
            self.efficiency_garrick_coefficients_2nd = (self.time_averaged_propulsive_force_coefficients_2nd * self.u_inf) / self.time_averaged_total_work_coefficients

