import copy
import math
import scipy
from models import model
import numpy as np
from utils import time_manager, input_data


class TheodorsenModel(model.Model):
    def __init__(self, theodorsen_input_data):
        self.initial_data = copy.deepcopy(theodorsen_input_data)
        self.scaled_data = input_data.theodorsen_garrick_scaling(self.initial_data)

        super().__init__(self.scaled_data)

        self.u = np.zeros(self.time_size, dtype=complex)
        self.u_dot = np.zeros(self.time_size, dtype=complex)

        self.h = np.zeros(self.time_size, dtype=complex)
        self.h_dot = np.zeros(self.time_size, dtype=complex)
        self.h_dot_2 = np.zeros(self.time_size, dtype=complex)

        self.theta = np.zeros(self.time_size, dtype=complex)
        self.theta_dot = np.zeros(self.time_size, dtype=complex)
        self.theta_dot_2 = np.zeros(self.time_size, dtype=complex)

        self.delta = np.zeros(self.time_size, dtype=complex)
        self.delta_dot = np.zeros(self.time_size, dtype=complex)
        self.delta_dot_2 = np.zeros(self.time_size, dtype=complex)

        self.K = None
        self.J = None
        self.D = None
        self.G = None
        self.F = None
        self.C = None

        self.t1 = None
        self.t2 = None
        self.t3 = None
        self.t4 = None
        self.t5 = None
        self.t6 = None
        self.t7 = None
        self.t8 = None
        self.t9 = None
        self.t10 = None
        self.t11 = None
        self.t12 = None
        self.t13 = None
        self.t14 = None
        self.t15 = None
        self.t16 = None
        self.t17 = None
        self.t18 = None
        self.t19 = None
        self.t20 = None

        self.Q = np.zeros(self.time_size, dtype=complex)

        self.lift_force_airfoil_complex = np.zeros(self.time_size, dtype=complex)
        self.lift_force_airfoil_real = np.zeros(self.time_size)

        self.lift_force_aileron_complex = np.zeros(self.time_size, dtype=complex)
        self.lift_force_aileron_real = np.zeros(self.time_size)

        self.moment_theta_complex = np.zeros(self.time_size, dtype=complex)
        self.moment_theta_real = np.zeros(self.time_size)

        self.moment_delta_complex = np.zeros(self.time_size, dtype=complex)
        self.moment_delta_real = np.zeros(self.time_size)

        self.compute_theodorsen_coefficients()

    def compute_theodorsen_coefficients(self):
        self.F = F(self.k)
        self.G = G(self.k)
        self.C = C(self.k)

        self.t1 = (- (1 / 3) * (2 + (self.e ** 2)) * np.sqrt(1 - (self.e ** 2)) + self.e * np.arccos(self.e))

        self.t2 = self.e * (1.0 - (self.e ** 2)) - (1.0 + (self.e ** 2)) * np.sqrt(1.0 - (self.e ** 2)) * np.arccos(self.e) + self.e * (np.arccos(self.e) ** 2)

        self.t3 = (- (1 / 8) * (1 - (self.e ** 2)) * (5 * (self.e ** 2) + 4)
                   + (1 / 4) * self.e * (7 + 2 * (self.e ** 2)) * np.sqrt(1 - (self.e ** 2)) * np.arccos(self.e)
                   - (1 / 8 + (self.e ** 2)) * ((np.arccos(self.e)) ** 2))

        self.t4 = (- np.arccos(self.e) + self.e * np.sqrt(1 - (self.e ** 2)))

        self.t5 = (- (1 - (self.e ** 2))
                   + 2 * self.e * np.sqrt(1 - (self.e ** 2)) * np.arccos(self.e)
                   - (np.arccos(self.e)) ** 2)

        self.t7 = ((1 / 8) * self.e * (7 + 2 * (self.e ** 2)) * np.sqrt(1 - (self.e ** 2))
                   - (1 / 8 + (self.e ** 2)) * np.arccos(self.e))

        self.t8 = - (1 / 3) * (1 + 2 * (self.e ** 2)) * np.sqrt(1 - (self.e ** 2)) + self.e * np.arccos(self.e)

        self.t9 = (1 / 2) * ((1 / 3) * ((1 - self.e ** 2) ** (3 / 2)) + self.a * self.t4)

        self.t10 = (np.sqrt(1 - self.e ** 2)
                    + np.arccos(self.e))

        self.t11 = (np.arccos(self.e) * (1 - 2 * self.e)
                    + np.sqrt(1 - self.e ** 2) * (2 - self.e))

        self.t12 = (- np.arccos(self.e) * (1 + 2 * self.e)
                    + np.sqrt(1 - self.e ** 2) * (2 + self.e))

        self.t13 = - (1 / 2) * (self.t7 + (self.e - self.a) * self.t1)

        self.t14 = (1 / 16) + (1 / 2) * self.a * self.e

        self.t6 = self.t2

        # Initially not in Theodorsen Paper but derived by Garrick to simply the equations.
        self.t15 = (1.0 + self.e) * np.sqrt(1 - (self.e ** 2))

        self.t16 = (self.t1
                    - self.t8
                    - (self.e - self.a) * self.t4
                    + (1 / 2) * self.t11)

        self.t17 = (- 2.0 * self.t9
                    - self.t1
                    + (self.a - 1 / 2) * self.t4)

        self.t18 = self.t5 - self.t4 * self.t10

        self.t19 = self.t4 * self.t11

        self.t20 = - np.sqrt(1 - (self.e ** 2)) + np.arccos(self.e)

    def movements_manager(self, time_index, func=None):
        self.u[time_index] = self.u_inf * (1.0 + 1.0j)

        self.h[time_index] = self.h_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_2)) + 1j * self.h_m
        self.h_dot[time_index] = (self.h_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_2))
        self.h_dot_2[time_index] = - self.h_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_2))

        self.theta[time_index] = self.theta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_0)) + 1j * self.theta_m
        self.theta_dot[time_index] = (self.theta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))
        self.theta_dot_2[time_index] = - self.theta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))

        self.delta[time_index] = self.delta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_1)) + 1j * self.delta_m
        self.delta_dot[time_index] = (self.delta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))
        self.delta_dot_2[time_index] = - self.delta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))

        self.alpha_eff[time_index] = self.theta[time_index].imag + np.arctan(self.h_dot[time_index].imag / self.u_inf)

    def movements_manager_based_pitching(self, time_index):
        self.theta[time_index] = self.theta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_0)) + 1j * self.theta_m
        self.theta_dot[time_index] = (self.theta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))
        self.theta_dot_2[time_index] = - self.theta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))

        self.delta[time_index] = self.delta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_1)) + 1j * self.delta_m
        self.delta_dot[time_index] = (self.delta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))
        self.delta_dot_2[time_index] = - self.delta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))

        self.alpha_eff[time_index] = self.theta[time_index].imag + np.arctan(self.h_dot[time_index].imag / self.u_inf)

        mass = 5.0
        if time_index != 0:
            self.h_dot_2[time_index] = (self.lift_force_total[time_index - 1] / mass) * 1j

            self.h_dot[time_index] = ((self.lift_force_total[time_index - 1] / mass) * self.time_step
                                      + self.h_dot[time_index - 1]) * 1j

            self.h[time_index] = (0.5 * (self.lift_force_total[time_index - 1] / mass) * (self.time_step ** 2)
                                  + self.h_dot[time_index - 1] * self.time_step
                                  + self.h[time_index - 1]) * 1j

    def compute_forces(self, time_index):
        self.Q[time_index] = (self.u_inf * self.theta[time_index]
                              + self.h_dot[time_index]
                              + self.b * ((1 / 2) - self.a) * self.theta_dot[time_index]
                              + (self.t10 / math.pi) * self.u_inf * self.delta[time_index]
                              + (self.b / (2 * math.pi)) * self.t11 * self.delta_dot[time_index])

        self.lift(time_index)
        self.lift_real(time_index)
        self.moments(time_index)
        self.moments_real(time_index)

    def lift(self, time_index):
        self.lift_force_airfoil_complex[time_index] = ((- self.rho * (self.b ** 2)) * (
                                    self.u_inf * math.pi * self.theta_dot[time_index]
                                    + math.pi * self.h_dot_2[time_index]
                                    - math.pi * self.b * self.a * self.theta_dot_2[time_index]
                                    - self.u_inf * self.t4 * self.delta_dot[time_index]
                                    - self.t1 * self.b * self.delta_dot_2[time_index])
                                                       - 2 * math.pi * self.rho * self.u_inf * self.b * self.C * self.Q[time_index])

        self.lift_force_aileron_complex[time_index] = ((- self.rho * (self.b ** 2)) * (
                                                    - self.u_inf * self.t4 * self.theta_dot[time_index]
                                                    - self.t4 * self.h_dot_2[time_index]
                                                    + self.b * self.t9 * self.theta_dot_2[time_index]
                                                    - (1.0 / (2.0 * math.pi)) * self.u_inf * self.t5 * self.delta_dot[time_index]
                                                    - (self.b / (2.0 * math.pi)) * self.t2 * self.delta_dot_2[time_index])

                                                       - (2.0 * self.rho * self.b * self.u_inf * np.sqrt(1 - (self.e ** 2))) * (
                                           (self.b / 2.0) * (1 - self.e) * self.theta_dot[time_index]
                                           + (1.0 / math.pi) * self.u_inf * np.sqrt(1 - (self.e ** 2)) * self.delta[time_index]
                                           + (self.b / (2.0 * math.pi)) * (1 - self.e) * self.t10 * self.delta_dot[time_index])

                                                       - (2.0 * self.rho * self.b * self.u_inf * self.t20 * self.C * self.Q[time_index]))

        self.lift_force_airfoil[time_index] = self.lift_force_airfoil_complex[time_index].imag
        self.lift_force_aileron[time_index] = self.lift_force_aileron_complex[time_index].imag
        self.lift_force_total[time_index] = self.lift_force_airfoil[time_index] + self.lift_force_aileron[time_index]

        self.lift_coefficient[time_index] = (self.lift_force_total[time_index] / (self.dynamic_pressure * self.c))

    def lift_real(self, time_index):
        self.lift_force_airfoil_real[time_index] = ((- self.rho * (self.b ** 2)) * (self.u_inf * math.pi * self.theta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                    - math.pi * self.h_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_2)
                                                                                    + math.pi * self.b * self.a * self.theta_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                    - self.u_inf * self.t4 * self.delta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_1)
                                                                                    + self.t1 * self.b * self.delta_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_1))
                                                    - 2.0 * math.pi * self.rho * self.u_inf * self.b * self.F * (self.u_inf * self.theta_0 * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                                                 + self.h_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_2)
                                                                                                                 + self.b * (1 / 2 - self.a) * self.theta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                                                 + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.sin(self.w * self.time[time_index] + self.phi_1)
                                                                                                                 + (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_1))
                                                    - 2.0 * math.pi * self.rho * self.u_inf * self.b * self.G * (self.u_inf * self.theta_0 * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                                                 - self.h_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_2)
                                                                                                                 - self.b * (1 / 2 - self.a) * self.theta_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                                                 + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.cos(self.w * self.time[time_index] + self.phi_1)
                                                                                                                 - (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_1))
                                                    )

        self.lift_force_aileron_real[time_index] = ((- self.rho * (self.b ** 2)) * (- self.u_inf * self.t4 * self.theta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                    + self.t4 * self.h_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_2)
                                                                                    - self.b * self.t9 * self.theta_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                    - (1.0 / (2.0 * math.pi)) * self.u_inf * self.t5 * self.delta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_1)
                                                                                    + (self.b / (2.0 * math.pi)) * self.t2 * self.delta_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_1))
                                                    - (2.0 * self.rho * self.b * self.u_inf * np.sqrt(1.0 - (self.e ** 2))) * ((self.b / 2.0) * (1.0 - self.e) * self.theta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                                                               + (1.0 / math.pi) * self.u_inf * np.sqrt(1.0 - (self.e ** 2)) * self.delta_0 * np.sin(self.w * self.time[time_index] + self.phi_1)
                                                                                                                               + (self.b / (math.pi * 2.0)) * (1.0 - self.e) * self.t10 * self.delta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_1))
                                                    - (2.0 * self.rho * self.u_inf * self.b * self.t20 * self.F) * (self.u_inf * self.theta_0 * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                                                    + self.h_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_2)
                                                                                                                    + self.b * (1.0 / 2.0 - self.a) * self.theta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                                                    + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.sin(self.w * self.time[time_index] + self.phi_1)
                                                                                                                    + (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_1))
                                                    - (2.0 * self.rho * self.u_inf * self.b * self.t20 * self.G) * (self.u_inf * self.theta_0 * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                                                    - self.h_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_2)
                                                                                                                    - self.b * ((1.0 / 2.0) - self.a) * self.theta_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                                                    + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.cos(self.w * self.time[time_index] + self.phi_1)
                                                                                                                    - (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_1))
                                                    )

    def moments(self, time_index):
        self.moment_theta_complex[time_index] = ((- self.rho * (self.b ** 2)) * (
                                                math.pi * (1 / 2 - self.a) * self.u_inf * self.b * self.theta_dot[time_index]
                                                + math.pi * (self.b ** 2) * (1 / 8 + (self.a ** 2)) * self.theta_dot_2[time_index]
                                                + self.t15 * (self.u_inf ** 2) * self.delta[time_index]
                                                + self.t16 * self.u_inf * self.b * self.delta_dot[time_index]
                                                + 2.0 * self.t13 * (self.b ** 2) * self.delta_dot_2[time_index]
                                                - self.a * math.pi * self.b * self.h_dot_2[time_index])
                                                 + 2.0 * self.rho * self.u_inf * (self.b ** 2) * math.pi * (self.a + 1 / 2) * C(self.k) * self.Q[time_index])

        self.moment_delta_complex[time_index] = ((- self.rho * (self.b ** 2)) * (
                                                self.t17 * self.u_inf * self.b * self.theta_dot[time_index]
                                                + 2.0 * self.t13 * (self.b ** 2) * self.theta_dot_2[time_index]
                                                + (1 / math.pi) * (self.u_inf ** 2) * self.t18 * self.delta[time_index]
                                                - (1 / (2.0 * math.pi)) * self.u_inf * self.b * self.t19 * self.delta_dot[time_index]
                                                - (1 / math.pi) * self.t3 * (self.b ** 2) * self.delta_dot_2[time_index]
                                                - self.t1 * self.b * self.h_dot_2[time_index])
                                                 - self.rho * self.u_inf * (self.b ** 2) * self.t12 * C(self.k) * self.Q[time_index])

        self.moment_airfoil[time_index] = self.moment_theta_complex[time_index].imag
        self.moment_aileron[time_index] = self.moment_delta_complex[time_index].imag

        self.moment_airfoil_coefficient[time_index] = self.moment_theta_complex[time_index].imag / (self.dynamic_pressure * (self.c ** 2))
        self.moment_aileron_coefficient[time_index] = self.moment_delta_complex[time_index].imag / (self.dynamic_pressure * (self.c ** 2))

    def moments_real(self, time_index):
        self.moment_theta_real[time_index] = ((- self.rho * (self.b ** 2)) * (math.pi * (1 / 2 - self.a) * self.u_inf * self.b * self.theta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                              - math.pi * (self.b ** 2) * (1 / 8 + (self.a ** 2)) * self.theta_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                              + self.t15 * (self.u_inf ** 2) * self.delta_0 * np.sin(self.w * self.time[time_index] + self.phi_1)
                                                                              + self.t16 * self.u_inf * self.b * self.delta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_1)
                                                                              - 2.0 * self.t13 * (self.b ** 2) * self.delta_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_1)
                                                                              + self.a * math.pi * self.b * self.h_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_2))
                                              + (2.0 * self.rho * self.u_inf * (self.b ** 2) * math.pi * (self.a + 1 / 2)) * self.F * (self.u_inf * self.theta_0 * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                                                                       + self.h_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_2)
                                                                                                                                       + self.b * (1 / 2 - self.a) * self.theta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                                                                       + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.sin(self.w * self.time[time_index] + self.phi_1)
                                                                                                                                       + (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_1))
                                              + (2.0 * self.rho * self.u_inf * (self.b ** 2) * math.pi * (self.a + 1 / 2)) * self.G * (self.u_inf * self.theta_0 * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                                                                       - self.h_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_2)
                                                                                                                                       - self.b * (1 / 2 - self.a) * self.theta_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                                                                       + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.cos(self.w * self.time[time_index] + self.phi_1)
                                                                                                                                       - (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_1))
                                                )

        self.moment_delta_real[time_index] = ((- self.rho * (self.b ** 2)) * (self.t17 * self.u_inf * self.b * self.theta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                              - 2.0 * self.t13 * (self.b ** 2) * self.theta_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                              + (1.0 / math.pi) * (self.u_inf ** 2) * self.t18 * self.delta_0 * np.sin(self.w * self.time[time_index] + self.phi_1)
                                                                              - (1.0 / (2.0 * math.pi)) * self.u_inf * self.b * self.t19 * self.delta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_1)
                                                                              + (1.0 / math.pi) * self.t3 * (self.b ** 2) * self.delta_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_1)
                                                                              + self.t1 * self.b * self.h_0 * (self.w ** 2) * np.sin(self.w * self.time[time_index] + self.phi_2))
                                              - (self.rho * self.u_inf * (self.b ** 2) * self.t12) * self.F * (self.u_inf * self.theta_0 * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                                               + self.h_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_2)
                                                                                                               + self.b * (1 / 2 - self.a) * self.theta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                                               + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.sin(self.w * self.time[time_index] + self.phi_1)
                                                                                                               + (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.cos(self.w * self.time[time_index] + self.phi_1))
                                              - (self.rho * self.u_inf * (self.b ** 2) * self.t12) * self.G * (self.u_inf * self.theta_0 * np.cos(self.w * self.time[time_index] + self.phi_0)
                                                                                                               - self.h_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_2)
                                                                                                               - self.b * (1 / 2 - self.a) * self.theta_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_0)
                                                                                                               + (self.t10 / math.pi) * self.u_inf * self.delta_0 * np.cos(self.w * self.time[time_index] + self.phi_1)
                                                                                                               - (self.t11 / (2.0 * math.pi)) * self.b * self.delta_0 * self.w * np.sin(self.w * self.time[time_index] + self.phi_1))
                                              )

    def postprocessing(self):
        self.compute_time_averaged_quantities()
        self.compute_efficiency()

        self.time = self.time * (self.scaled_data.case.movement.frequency / self.initial_data.case.movement.frequency)
        self.time_size = np.size(self.time)
        self.period = self.period * (self.scaled_data.case.movement.frequency / self.initial_data.case.movement.frequency)
        self.time_over_period = self.time / self.period
        self.time_step = self.time_step * (self.scaled_data.case.movement.frequency / self.initial_data.case.movement.frequency)


def G(k):
    if k == 0.0:
        g_value = 0.0
    else:
        g_value = - ((scipy.special.yv(1, k) * scipy.special.yv(0, k)
                      + scipy.special.jv(1, k) * scipy.special.jv(0, k))
                     / (((scipy.special.jv(1, k) + scipy.special.yv(0, k)) ** 2)
                        + ((scipy.special.yv(1, k) - scipy.special.jv(0, k)) ** 2)))
    return g_value


def F(k):
    if k == 0.0:
        f_value = 0.0
    else:
        f_value = ((scipy.special.jv(1, k) * (scipy.special.jv(1, k) + scipy.special.yv(0, k))
                    + scipy.special.yv(1, k) * (scipy.special.yv(1, k) - scipy.special.jv(0, k)))
                   / (((scipy.special.jv(1, k) + scipy.special.yv(0, k)) ** 2)
                      + ((scipy.special.yv(1, k) - scipy.special.jv(0, k)) ** 2)))
    return f_value


def C(k):
    if k == 0.0:
        c_value = 0.0 + 1j * 0.0
    else:
        c_value = scipy.special.hankel2(1, k) / (1j * scipy.special.hankel2(0, k) + scipy.special.hankel2(1, k))

    return c_value
