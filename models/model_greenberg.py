import math
import numpy as np
from models import model, model_theodorsen


class GreenbergModel(model.Model):
    def __init__(self, greenberg_input_data):
        self.initial_data = greenberg_input_data

        super().__init__(greenberg_input_data)

        self.u = np.zeros(self.time_size, dtype=complex)
        self.u_dot = np.zeros(self.time_size, dtype=complex)

        self.h = np.zeros(self.time_size, dtype=complex)
        self.h_dot = np.zeros(self.time_size, dtype=complex)
        self.h_dot_2 = np.zeros(self.time_size, dtype=complex)

        self.theta = np.zeros(self.time_size, dtype=complex)
        self.theta_dot = np.zeros(self.time_size, dtype=complex)
        self.theta_dot_2 = np.zeros(self.time_size, dtype=complex)

        self.delta = np.zeros(self.time_size, dtype=complex)
        self.delta_dot = np.zeros(self.time_size, dtype=complex)
        self.delta_dot_2 = np.zeros(self.time_size, dtype=complex)

        self.surge_amplitude = greenberg_input_data.case.flow.surge_amplitude

        self.theta_m = greenberg_input_data.case.movement.mean_pitching_amplitude
        self.delta_m = greenberg_input_data.case.movement.mean_tail_pitching_amplitude

        self.f_surge = greenberg_input_data.case.flow.surge_frequency
        self.w_surge = greenberg_input_data.case.flow.surge_pulsation
        self.k_surge = greenberg_input_data.case.flow.surge_reduced_frequency
        self.phi_surge = - math.pi / 2.0

        self.k_greenberg = ((self.w + self.w_surge) * self.b) / self.u_inf

        self.theta_oscillation = np.zeros(self.time_size, dtype=complex)

        self.lift_force_complex = np.zeros(self.time_size, dtype=complex)
        self.lift_force_wake_complex = np.zeros(self.time_size, dtype=complex)
        self.lift_force_aileron_complex = np.zeros(self.time_size, dtype=complex)
        self.lift_force_total_complex = np.zeros(self.time_size, dtype=complex)

        self.moment_noncirculatory_complex = np.zeros(self.time_size, dtype=complex)
        self.moment_wake_complex = np.zeros(self.time_size, dtype=complex)
        self.moment_aileron_complex = np.zeros(self.time_size, dtype=complex)
        self.moment_airfoil_complex = np.zeros(self.time_size, dtype=complex)

        self.lift_force_test = np.zeros(self.time_size)

    def movements_manager(self, time_index, func=None):
        self.u[time_index] = self.u_inf * (1.0 + 1.0j)

        self.h[time_index] = self.h_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_2)) + 1j * self.h_m
        self.h_dot[time_index] = (self.h_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_2))
        self.h_dot_2[time_index] = - self.h_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_2))

        self.theta[time_index] = self.theta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_0)) + 1j * self.theta_m
        self.theta_dot[time_index] = (self.theta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))
        self.theta_dot_2[time_index] = - self.theta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))

        self.delta[time_index] = self.delta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_1)) + 1j * self.delta_m
        self.delta_dot[time_index] = (self.delta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))
        self.delta_dot_2[time_index] = - self.delta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))

        self.alpha_eff[time_index] = self.theta[time_index].imag + np.arctan(self.h_dot[time_index].imag / self.u_inf)

        self.u[time_index] = self.u_inf * (1.0 + self.surge_amplitude * np.exp(1j * (self.w_surge * self.time[time_index] + self.phi_surge)))
        self.u_dot[time_index] = self.u_inf * self.surge_amplitude * 1j * self.w_surge * np.exp(1j * (self.w_surge * self.time[time_index] + self.phi_surge))

        self.theta_oscillation[time_index] = self.theta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))

    def movements_manager_based_pitching(self, time_index):
        self.theta[time_index] = self.theta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_0)) + 1j * self.theta_m
        self.theta_dot[time_index] = (self.theta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))
        self.theta_dot_2[time_index] = - self.theta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))

        self.delta[time_index] = self.delta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_1)) + 1j * self.delta_m
        self.delta_dot[time_index] = (self.delta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))
        self.delta_dot_2[time_index] = - self.delta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))

        self.alpha_eff[time_index] = self.theta[time_index].imag + np.arctan(self.h_dot[time_index].imag / self.u_inf)

        mass = 5.0
        if time_index != 0:
            self.h_dot_2[time_index] = (self.lift_force_total[time_index - 1] / mass) * 1j

            self.h_dot[time_index] = ((self.lift_force_total[time_index - 1] / mass) * self.time_step
                                      + self.h_dot[time_index - 1]) * 1j

            self.h[time_index] = (0.5 * (self.lift_force_total[time_index - 1] / mass) * (self.time_step ** 2)
                                  + self.h_dot[time_index - 1] * self.time_step
                                  + self.h[time_index - 1]) * 1j

    def compute_forces(self, time_index):
        self.lift(time_index)
        self.moment(time_index)

    def lift(self, time_index):
        self.lift_force_complex[time_index] = ((- math.pi * self.rho * (self.b ** 2)) * (self.h_dot_2[time_index]
                                                                                         + self.u[time_index] * self.theta_dot[time_index]
                                                                                         + self.u_dot[time_index] * (self.theta_m + self.theta_oscillation[time_index])
                                                                                         - self.b * self.a * self.theta_dot_2[time_index]))

        self.lift_force_aileron_complex[time_index] = 0.0 + 0.0j

        self.lift_force_wake_complex[time_index] = (- 2.0 * math.pi * self.rho * self.b * self.u[time_index] * (self.u_inf * self.theta_m
                                                                                                                + self.surge_amplitude * self.u_inf * self.theta_m * model_theodorsen.C(self.k_surge) * np.exp(1j * (self.w_surge * self.time[time_index] + self.phi_surge))
                                                                                                                + (1j * self.w * self.theta_0 * self.b * (1 / 2 - self.a) + self.u_inf * self.theta_0) * model_theodorsen.C(self.k) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))
                                                                                                                + 1j * self.w * self.h_0 * model_theodorsen.C(self.k) * np.exp(1j * (self.w * self.time[time_index] + self.phi_2))
                                                                                                                + self.surge_amplitude * self.u_inf * self.theta_0 * model_theodorsen.C(self.k_greenberg) * np.exp(1j * ((self.w + self.w_surge) * self.time[time_index] + self.phi_0 + self.phi_surge))))

        self.lift_force_total_complex[time_index] = (self.lift_force_complex[time_index]
                                                     + self.lift_force_aileron_complex[time_index]
                                                     + self.lift_force_wake_complex[time_index])

        self.lift_force_total[time_index] = self.lift_force_total_complex[time_index].real
        self.lift_force_airfoil[time_index] = self.lift_force_complex[time_index].real + self.lift_force_wake_complex[time_index].real
        self.lift_force_aileron[time_index] = self.lift_force_aileron_complex[time_index].real

        self.lift_coefficient[time_index] = self.lift_force_total[time_index] / (self.dynamic_pressure * self.c)

        self.lift_force_test[time_index] = (- math.pi * self.rho * (self.b ** 2) * self.u_dot[time_index].real * self.theta_m
                                            - 2.0 * math.pi * self.rho * self.b * self.u[time_index].real * (self.u_inf * self.theta_m
                                                                                                                + self.surge_amplitude * self.u_inf * self.theta_m * model_theodorsen.C(self.k_surge) * np.exp(1j * (self.w_surge * self.time[time_index] + self.phi_surge))).real)


    def moment(self, time_index):
        self.moment_noncirculatory_complex[time_index] = ((- math.pi * self.rho * (self.b ** 2)) * (
                - (self.u[time_index] ** 2) * (self.theta_m + self.theta_oscillation[time_index])
                - self.b * self.a * self.u_dot[time_index] * (self.theta_m + self.theta_oscillation[time_index])
                - self.u[time_index] * self.h_dot[time_index]
                + (self.b ** 2) * ((1 / 8) + (self.a ** 2)) * self.theta_dot_2[time_index]))

        self.moment_wake_complex[time_index] = (- math.pi * self.rho * (self.b ** 2) * self.u[time_index] * (self.h_dot[time_index]
                                                                                                             + self.u[time_index] * (self.theta_m + self.theta_oscillation[time_index])
                                                                                                             + self.theta_dot[time_index] * self.b * ((1 / 2) - self.a))
                                                + 2.0 * math.pi * self.rho * (self.b ** 2) * self.u[time_index] * (self.a + (1 / 2)) * (self.u_inf * self.theta_m
                                                                                                                                        + self.surge_amplitude * self.u_inf * self.theta_m * model_theodorsen.C(self.k_surge) * np.exp(1j * (self.w_surge * self.time[time_index] + self.phi_surge))
                                                                                                                                        + (self.b * ((1 / 2) - self.a) * self.theta_dot[time_index] + self.u_inf * self.theta_oscillation[time_index]) * model_theodorsen.C(self.k)
                                                                                                                                        + self.h_dot[time_index] * model_theodorsen.C(self.k)
                                                                                                                                        + self.surge_amplitude * self.u_inf * self.theta_oscillation[time_index] * model_theodorsen.C(self.k_greenberg) * np.exp(1j * (self.w_surge * self.time[time_index] + self.phi_surge)))
                                                )

        self.moment_airfoil_complex[time_index] = self.moment_noncirculatory_complex[time_index] + self.moment_wake_complex[time_index]
        self.moment_aileron_complex[time_index] = 0.0 + 0.0j

        self.moment_airfoil[time_index] = self.moment_airfoil_complex[time_index].real
        self.moment_aileron[time_index] = self.moment_aileron_complex[time_index].real

        self.moment_airfoil_coefficient[time_index] = self.moment_airfoil[time_index] / (self.dynamic_pressure * (self.c ** 2))
        self.moment_aileron_coefficient = self.moment_aileron[time_index] / (self.dynamic_pressure * (self.c ** 2))
