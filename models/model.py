import numpy as np
from utils import time_manager


class Model:
    def __init__(self, input_data):
        self.time_manager = time_manager.TimeManager(input_data.case)

        self.time = self.time_manager.time

        self.period = self.time_manager.period

        if self.period is not None:
            self.time_over_period = self.time / self.period

        self.phi_0 = input_data.case.movement.pitching_phase
        self.phi_1 = input_data.case.movement.tail_pitching_phase
        self.phi_2 = input_data.case.movement.heaving_phase

        self.f = input_data.case.movement.frequency
        self.w = input_data.case.movement.angular_frequency
        self.k = input_data.case.movement.reduced_frequency

        self.h_0 = input_data.case.movement.heaving_amplitude
        self.h_m = input_data.case.movement.mean_heaving_amplitude

        self.theta_0 = input_data.case.movement.pitching_amplitude
        self.theta_m = input_data.case.movement.mean_pitching_amplitude

        self.delta_0 = input_data.case.movement.tail_pitching_amplitude
        self.delta_m = input_data.case.movement.mean_tail_pitching_amplitude

        self.time_step = self.time_manager.dt
        self.time_size = np.size(self.time)

        self.rho = input_data.case.flow.rho
        self.temperature = input_data.case.flow.temperature
        self.dynamic_viscosity = input_data.case.flow.dynamic_viscosity

        self.e = input_data.case.airfoil.aileron_starting_position
        self.a = input_data.case.airfoil.rotation_axis_position
        self.rotation_axis_in_percentage_chord = input_data.case.airfoil.rotation_axis_in_percentage_chord
        self.b = input_data.case.airfoil.semi_chord
        self.c = input_data.case.airfoil.chord

        self.u_inf = input_data.case.flow.freestream_velocity
        self.dynamic_pressure = (1 / 2) * self.rho * (self.u_inf ** 2)

        if input_data.case.movement.frequency is not None:
            self.f_star = (input_data.case.movement.frequency * self.b) / self.u_inf

        self.time_dimensionless = (self.time * self.u_inf) / self.c

        self.u = np.zeros(self.time_size)

        self.reynolds_number = input_data.case.flow.reynolds_number

        self.lift_force_total = np.zeros(self.time_size)
        self.lift_force_airfoil = np.zeros(self.time_size)
        self.lift_force_aileron = np.zeros(self.time_size)
        self.lift_coefficient = np.zeros(self.time_size)

        self.drag_force_total = np.zeros(self.time_size)
        self.drag_force_airfoil = np.zeros(self.time_size)
        self.drag_force_aileron = np.zeros(self.time_size)
        self.drag_coefficient = np.zeros(self.time_size)

        self.propulsive_force_total = np.zeros(self.time_size)
        self.propulsive_force_airfoil = np.zeros(self.time_size)
        self.propulsive_force_aileron = np.zeros(self.time_size)
        self.propulsive_coefficient = np.zeros(self.time_size)

        self.moment_airfoil = np.zeros(self.time_size)
        self.moment_aileron = np.zeros(self.time_size)
        self.moment_airfoil_coefficient = np.zeros(self.time_size)
        self.moment_aileron_coefficient = np.zeros(self.time_size)

        self.h = np.zeros(self.time_size)
        self.h_dot = np.zeros(self.time_size)
        self.h_dot_2 = np.zeros(self.time_size)

        self.theta = np.zeros(self.time_size)
        self.theta_dot = np.zeros(self.time_size)
        self.theta_dot_2 = np.zeros(self.time_size)

        self.delta = np.zeros(self.time_size)
        self.delta_dot = np.zeros(self.time_size)
        self.delta_dot_2 = np.zeros(self.time_size)

        self.alpha_eff = np.zeros(self.time_size)

    def movements_manager(self, time_index, func=None):
        pass

    def compute_forces(self, time_index):
        pass

    def compute_time_averaged_quantities(self):
        pass

    def compute_efficiency(self):
        pass

    def compute_forces_batch(self):
        pass

    def postprocessing(self):
        pass
