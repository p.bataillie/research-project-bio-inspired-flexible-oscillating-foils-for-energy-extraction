import os
from models.ldvm_files import LDVM
from models import model
import numpy as np
from utils import filter


class LDVMModel(model.Model):
    def __init__(self, LDVM_input_data):
        self.initial_data = LDVM_input_data

        super().__init__(self.initial_data)

        self.critical_lesp = self.initial_data.case.airfoil.critical_lesp

        self.ldvm_obj = LDVM.LDVM(project_dir=os.getcwd() + '/',
                                  ldvm_dir='models/ldvm_files/')

        self.ldvm_obj.setup_inputs(chord=self.c,
                                   freestream_velocity=self.u_inf,
                                   pitching_axis_position=self.rotation_axis_in_percentage_chord,
                                   airfoil_name="naca0015",
                                   reynolds_number=self.reynolds_number,
                                   critical_lesp=self.critical_lesp,
                                   output_frequency=1)

        self.lift_force_airfoil_real = np.zeros(self.time_size)
        self.lift_force_aileron_real = np.zeros(self.time_size)
        self.moment_theta_real = np.zeros(self.time_size)
        self.moment_delta_real = np.zeros(self.time_size)

    def movements_manager(self, time_index, func=None):
        if func is None:
            self.u[time_index] = self.u_inf * (1.0 + 1.0j).imag

            self.h[time_index] = (self.h_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_2)) + 1j * self.h_m).imag
            self.h_dot[time_index] = ((self.h_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_2))).imag
            self.h_dot_2[time_index] = (- self.h_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_2))).imag

            self.theta[time_index] = (self.theta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_0)) + 1j * self.theta_m).imag
            self.theta_dot[time_index] = ((self.theta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))).imag
            self.theta_dot_2[time_index] = (- self.theta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_0))).imag

            self.delta[time_index] = (self.delta_0 * np.exp(1j * (self.w * self.time[time_index] + self.phi_1)) + 1j * self.delta_m).imag
            self.delta_dot[time_index] = ((self.delta_0 * 1j * self.w) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))).imag
            self.delta_dot_2[time_index] = (- self.delta_0 * (self.w ** 2) * np.exp(1j * (self.w * self.time[time_index] + self.phi_1))).imag

            self.alpha_eff[time_index] = (self.theta[time_index].imag + np.arctan(self.h_dot[time_index].imag / self.u_inf)).imag

        else:
            self.theta[time_index], self.h[time_index] = func(self.time[time_index])
            self.u[time_index] = self.u_inf


    def compute_forces_batch(self):
        self.ldvm_obj.setup_kinematics(non_dimensional_time=self.time_dimensionless,
                                       theta_in_degrees=np.degrees(self.theta),
                                       h_c=-(self.h / self.c))

        self.ldvm_obj.run(show_output=False)

        file_data = self.ldvm_obj.output_data

        # time = file_data[:, 0]
        # theta = file_data[:, 1]
        # h = file_data[:, 2]
        # u = file_data[:, 3]
        # gamma = file_data[:, 4]
        # lesp = file_data[:, 5]
        # c_n = file_data[:, 6]
        # c_s = file_data[:, 7]

        skip = 5
        size = np.size(self.time)
        cl = - file_data[skip:, 8]
        cd = file_data[skip:, 9]
        cm = file_data[skip:, 10]

        for i in range(np.abs(size - np.size(cl))):
            cl = np.insert(cl, 0, np.nan)
            cd = np.insert(cd, 0, np.nan)
            cm = np.insert(cm, 0, np.nan)

        if self.time_manager.duration is None:
            self.time_manager.duration = self.period

        cl = filter.lowpass_filter(cl, (1.0 / self.time_manager.duration) * 75.0, sample_rate=(1.0 / self.time_step))
        cd = filter.lowpass_filter(cd, (1.0 / self.time_manager.duration) * 75.0, sample_rate=(1.0 / self.time_step))
        cm = filter.lowpass_filter(cm, (1.0 / self.time_manager.duration) * 75.0, sample_rate=(1.0 / self.time_step))

        self.lift_coefficient = cl
        self.drag_coefficient = cd
        self.moment_airfoil_coefficient = cm

        self.lift_force_airfoil = self.lift_coefficient * self.dynamic_pressure * self.c
        self.lift_force_total = self.lift_force_airfoil

        self.drag_force_airfoil = self.drag_coefficient * self.dynamic_pressure * self.c
        self.drag_force_total = self.drag_force_airfoil

        self.propulsive_force_total = - self.drag_force_total
        self.propulsive_force_airfoil = - self.drag_force_airfoil
        self.propulsive_coefficient = - self.drag_coefficient

        self.moment_airfoil = self.moment_airfoil_coefficient * (self.dynamic_pressure * (self.c ** 2))

