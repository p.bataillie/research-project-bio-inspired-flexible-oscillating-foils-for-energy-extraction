from utils import solver

solver_obj = solver.Solver(filepath="cases/experimental.json", case_set="f_0_8-pitching_amplitude_2")
solver_obj.solve()
results = solver_obj.output()

# results.show_movement(show_plot=False)

data = [[[results[0].time_over_period, results[0].lift_coefficient, 'num', 'Theodorsen - FP', 'red'], [results[1].time_over_period, results[1].lift_coefficient, 'num', 'LDVM - NACA0015', 'green'], [results[0].time_over_period_exp, results[0].lift_coefficient_exp, 'exp', 'Exp. Data - FP', 'black']],
        [[results[0].time_over_period, results[0].drag_coefficient, 'num', 'Garrick - FP', 'red'], [results[1].time_over_period, results[1].drag_coefficient, 'num', 'LDVM - NACA0015', 'green'], [results[0].time_over_period_exp, results[0].drag_coefficient_exp, 'exp', 'Exp. Data - FP', 'black']]]

axis_labels = [['$\\frac{t}{T}$', '$C_{L}$'], ['$\\frac{t}{T}$', '$C_{D}$']]

results[0].show_lift_drag_general(data,
                               axis_labels,
                               show_plot=True)
