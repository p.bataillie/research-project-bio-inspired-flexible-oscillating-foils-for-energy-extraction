import torch
import torch.nn as nn
import matplotlib.pyplot as plt


class MLP(nn.Module):
    def __init__(self, n_input, model):
        super().__init__()
        self.layers = nn.Sequential()
        self.layers.append(nn.Linear(n_input, model[0]))
        self.layers.append(nn.ReLU())
        for layer in range(len(model) - 1):
            self.layers.append(nn.Linear(model[layer], model[layer + 1]))
            self.layers.append(nn.ReLU())
        self.layers.append(nn.Linear(model[-1], 1))
        # self.dropout = nn.Dropout(0.001)

    def forward(self, x):
        out = self.layers(x)
        # out=self.dropout(out)
        return out

    def print_model(self):
        print(self.layers)

    def training(self, model, n_epoch, train_loader, test_loader, optimizer, loss_function, plot=True):
        # Run the training loop

        loss_history_tr = []
        loss_history_test = []

        for epoch in range(0, n_epoch):

            # Iterate over the DataLoader for training data
            for i, datas in enumerate(train_loader, 0):
                # Get and prepare inputs
                inputs, targets = datas
                inputs, targets = inputs.float(), targets.float()

                # Zero the gradients
                optimizer.zero_grad()

                # Perform forward pass
                outputs = self.forward(inputs)

                # Compute loss
                loss = loss_function(outputs, targets)

                # Perform backward pass
                loss.backward()

                # Perform optimization
                optimizer.step()

            loss_history_tr.append(float(loss))  # add loss at the given epoch to loss history

            loss_test = 0
            for j, test in enumerate(test_loader, 0):  # compute mean loss over the test dataset
                inputs_t, targets_t = test
                inputs_t, targets_t = inputs_t.float(), targets_t.float()
                outputs_t = self.forward(inputs_t)
                loss_test += loss_function(outputs_t, targets_t)
            loss_test = loss_test / j
            loss_history_test.append(float(loss_test))

            print("Epoch : %d - Train_loss = %f - Test_loss=%f" % (epoch, loss, loss_test))

        # plot training history
        if plot:
            epoch_list = [i for i in range(0, n_epoch)]
            plt.figure()
            ax = plt.subplot(111)
            plt.semilogy(epoch_list, loss_history_tr, label="Training set")
            plt.semilogy(epoch_list, loss_history_test, label="Testing")
            plt.grid()
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])

            # Put a legend to the right of the current axis
            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            plt.xlabel("Epochs")
            plt.ylabel("Loss")
            plt.title(f"Training {model}")
            plt.ylim(1e-4, 1)
            plt.show()


class Dataset(torch.utils.data.Dataset):
    # load the dataset
    def __init__(self, X, y):
        # store the inputs and outputs
        self.X = X
        self.y = y

    # number of rows in the dataset
    def __len__(self):
        return self.X.size()[0]

    # get a row at an index
    def __getitem__(self, idx):
        return [self.X[idx], self.y[idx]]
