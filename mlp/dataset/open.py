import numpy as np
import torch
from mlp import MLPmodel

outputs = torch.load('outputs/dataset_outputs.pt')
inputs = torch.load('outputs/dataset_inputs.pt')

def shuffle(input_tensor, output_tensor):
    tensor_1 = input_tensor.numpy()
    tensor_2 = output_tensor.numpy()

    # idx = np.arange(0, np.size(tensor_1, 0))
    idx = np.arange(0, 400) * 233
    np.random.shuffle(idx)

    inputs_shuffle = np.zeros((np.size(tensor_1, 0), np.size(tensor_1, 1)))
    outputs_shuffle = np.zeros((np.size(tensor_2, 0), np.size(tensor_2, 1)))

    for j in range(np.size(idx, 0)):
        for k in range(233):
            inputs_shuffle[idx[j] + k] = tensor_1[j + k]
            outputs_shuffle[idx[j] + k] = tensor_2[j + k]

    return torch.tensor(inputs_shuffle), torch.tensor(outputs_shuffle)

inputs_shuffle, outputs_shuffle = shuffle(inputs, outputs)

# for i in range(np.size(inputs_shuffle, 0)):
#     if np.array_equal(inputs_shuffle[i], inputs[0]):
#         print(i)

train_dataset = MLPmodel.Dataset(inputs_shuffle[:60000], outputs_shuffle[:60000])
test_dataset = MLPmodel.Dataset(inputs_shuffle[60001:70000], outputs_shuffle[60001:70000])
validation_dataset = MLPmodel.Dataset(inputs_shuffle[70001:], outputs_shuffle[70001:])

torch.save(train_dataset, 'train_dataset.pt')
torch.save(test_dataset, 'test_dataset.pt')
torch.save(validation_dataset, 'validation_dataset.pt')
