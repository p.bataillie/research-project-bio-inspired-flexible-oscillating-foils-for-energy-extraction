import matplotlib.pyplot as plt
import torch
from mlp import MLPmodel

nb_sample = 10

validation_dataset = torch.load("./validation_dataset.pt")

model = MLPmodel.MLP(12,[512, 512])
model.load_state_dict(torch.load("./lift_coefficient.pt"))
model.eval()

kinematics = validation_dataset.X[nb_sample * 233:nb_sample * 233 + 233]
lift_coefficient_ref = validation_dataset.y[nb_sample * 233:nb_sample * 233 + 233]

validation_dataset.y = None

size = [233, 1]
lift_coefficient = torch.zeros(size)
h = torch.zeros(size)
h_dot = torch.zeros(size)
theta = torch.zeros(size)
theta_dot = torch.zeros(size)

for i in range(lift_coefficient.shape[0]):
    theta[i] = kinematics[i, :][0].float()
    theta_dot[i] = kinematics[i, :][1].float()
    h[i] = kinematics[i, :][2].float()
    h_dot[i] = kinematics[i, :][3].float()
    lift_coefficient[i] = model(kinematics[i, :].float())

theta = theta.detach().numpy()
h = h.detach().numpy()
lift_coefficient = lift_coefficient.detach().numpy()
lift_coefficient_ref = lift_coefficient_ref.detach().numpy()

plt.figure()
plt.plot(lift_coefficient, label='MLP')
plt.plot(lift_coefficient_ref, label='LDVM', linestyle='dashed')
plt.grid()
plt.legend()
plt.show()