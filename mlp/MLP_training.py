"""
This file trains the feed-forward network
"""

import torch
import torch.nn as nn
import MLPmodel
import os


def Training(lr, n_epoch, model):
    # Dataset preparation
    train_dataset = torch.load("./dataset/train_dataset.pt")
    test_dataset = torch.load("./dataset/test_dataset.pt")

    batch_size = 256

    train_loader = torch.utils.data.DataLoader(dataset=train_dataset,
                                               batch_size=batch_size,
                                               shuffle=True)

    test_loader = torch.utils.data.DataLoader(dataset=test_dataset,
                                              batch_size=batch_size,
                                              shuffle=False)

    # Initialize the MLP
    mlp = MLPmodel.MLP(len(train_dataset.__getitem__(0)[0]), model)
    mlp.print_model()

    # Define the loss function and optimizer
    loss_function = nn.MSELoss()
    optimizer = torch.optim.Adam(mlp.parameters(), lr)

    # Training
    mlp.training(model, n_epoch, train_loader, test_loader, optimizer, loss_function)

    torch.save(mlp.state_dict(), "./dataset/lift_coefficient.pt")


if __name__ == "__main__":
    Training(5e-4, 200, [512, 512])
