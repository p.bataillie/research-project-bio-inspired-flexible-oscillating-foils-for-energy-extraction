#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Apr  7 15:27:48 2022

@author: baptiste

This file uses the trained neural net on the validation data and computes the associated R2s
 
"""

import torch
import matplotlib.pyplot as plt 
import MLPmodel 
import numpy as np
import sklearn.metrics
from matplotlib import rcParams
import math

rcParams.update({'figure.autolayout': True})

def frequency(phi_max,Re=1000, mu=1.745*10**-5, rho=1.2, c=0.01,AR = 4): 
    return Re*mu/(4*c*(c*AR)*rho*phi_max/180*math.pi)


def psi_point(alpha_max,phi_max,time):
    f=frequency(phi_max)
    return np.array([(90-alpha_max)/180*math.pi*2*math.pi*f*math.cos(2*math.pi*t) for t in time])
    # return[(((90-alpha_max)/180*math.pi*2*math.pi*f*math.cos(2*math.pi*t)))for t in time]


def phi_point(phi_max,delta,time):

    f=frequency(phi_max)

    return np.array([phi_max/180*math.pi*2*math.pi*f*math.cos(2*math.pi*time[t]+delta/180*math.pi) for t in range(len(time))])
    # return [(phi_max/180*math.pi*2*math.pi*f*math.cos(2*math.pi*time[t]+delta/180*math.pi)) for t in range(len(time))]







def Validate(layers,display_plots=False):
    errors=0
    validation_dataset=torch.load("../NNet_files/validation_set.pt")
    validation_index=np.load("../NNet_files/val_index.npy")

    n_dtxn_input=len(validation_dataset.__getitem__(0)[0]) #length of one snapshot

    model = MLPmodel.MLP(n_dtxn_input,layers)
    model.load_state_dict(torch.load("../NNet_files/revT.pt")) #loads trained model
    model.eval()

    mean=np.load("../NNet_files/mean_revT.npy") #useful to de-standardize the output
    rms=np.load("../NNet_files/rms_revT.npy")
    output_name="Revolution Torque"

    R2_memory=[]

    fig, axs = plt.subplots(5,figsize=(5, 15), dpi=80)
    plt.setp(axs, xticks=[242,492], xticklabels=['T', '2T'])
    displayed_sim=[0,5,3,10,20]
    fig.tight_layout()

    for j in range(len(validation_index)-1):

        Ypred=[model(validation_dataset.__getitem__(i)[0].float()).detach().numpy() for i in range(int(validation_index[j][0]),int(validation_index[j+1][0]))]
        Ytruth=[validation_dataset.__getitem__(i)[1].detach().numpy() for i in range(int(validation_index[j][0]),int(validation_index[j+1][0]))]
        Ypred=np.array(Ypred)*rms+mean
        Ytruth=np.array(Ytruth)*rms+mean
        error=(Ytruth-Ypred)
        t=[i for i in range(int(validation_index[j+1][0])-int(validation_index[j][0]))]
        R2_test= sklearn.metrics.r2_score(Ytruth,Ypred)

        R2_memory.append(R2_test)

        Ytruth=np.multiply(Ytruth[:,0],phi_point(validation_index[j][1],validation_index[j][2],np.linspace(0,2,493)))
        Ypred=np.multiply(Ypred[:,0],phi_point(validation_index[j][1],validation_index[j][2],np.linspace(0,2,493)))

        a=np.sum(Ytruth)/len(Ytruth)
        b=np.sum(Ypred)/len(Ypred)
        # print(a)
        # print(b)
        print((a-b)/a)
        errors+=abs((a-b)/a)

        if display_plots:
            if j in displayed_sim:
                axs[displayed_sim.index(j)].plot(t,Ytruth, label="Truth")
                axs[displayed_sim.index(j)].plot(t,Ypred,label="Prediction")
                axs[displayed_sim.index(j)].plot(t,error, label="Error")
                axs[displayed_sim.index(j)].set_title(f"$\\alpha=${validation_index[j][1]}°, $\\phi=${validation_index[j][2]}°, $\\delta=${validation_index[j][3]}°")
                axs[displayed_sim.index(j)].set_ylabel(output_name)
                axs[displayed_sim.index(j)].set_xlim([0,492])
            if j==0:
                axs[j].legend()

                plt.figure()
                ax = plt.subplot(111)
                plt.plot(t,Ytruth, label="Truth")
                plt.plot(t,Ypred,label="Prediction")
                plt.plot(t,error, label="Error")
                plt.grid()

                box = ax.get_position()
                ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])


                # Put a legend to the right of the current axis
                ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
                plt.xlabel("Timesteps")
                plt.ylabel(output_name)
                plt.title(f"Validation set n°{j+1}, $\\alpha=${validation_index[j][1]}, $\\phi=${validation_index[j][2]}, $\\delta=${validation_index[j][3]}  ")

                plt.figure(figsize=(5, 5))
                plt.plot(Ytruth, Ypred, 'k.')
                plt.plot(Ytruth, Ytruth, 'r--')
                plt.xlabel("Truth")
                plt.ylabel("Prediction")
                plt.title(f"Validation set n°{j+1}, $\\alpha=${validation_index[j][1]}, $\\phi=${validation_index[j][2]}, $\\delta=${validation_index[j][3]}  ")


            f,ax = plt.subplots(1,2,figsize=(10,4))
            f.tight_layout()
            ax[0].plot(t,Ytruth, label="Truth")
            ax[0].plot(t,Ypred,label="Prediction")
            ax[0].plot(t,error, label="Error")
            ax[0].grid()

            box = ax[0].get_position()
            ax[0].set_position([box.x0, box.y0, box.width * 0.8, box.height])

            # Put a legend to the right of the current axis
            ax[0].legend(loc='lower left',fontsize=7)

            ax[0].set_xlabel("Timesteps")
            ax[0].set_ylabel(output_name)


            ax[1].plot(Ytruth, Ypred, 'k.')
            ax[1].plot(Ytruth, Ytruth, 'r--')
            ax[1].set_xlabel("Truth")
            ax[1].set_ylabel("Prediction")
            plt.setp(ax[1], xticks=[], yticks=[])
            plt.suptitle(f"Validation set n°{j+1}, $\\alpha=${validation_index[j][1]}, $\\phi=${validation_index[j][2]}, $\\delta=${validation_index[j][3]}  ")
            f.tight_layout()
        print(f'The coefficient of determination for validation set n°{j+1} is R2 = {R2_test}')

    print(f"\n \n meanR2={sum(R2_memory)/len(R2_memory)}")
    print(errors/23)
    np.save("../NNet_files/R2.npy",np.array(R2_memory))
    # fig.savefig("valid_lift.png",dpi=300)
if __name__=="__main__":

    Validate([256,256],True)
    
    

