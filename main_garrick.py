from utils import solver

solver_obj = solver.Solver(filepath="cases/experimental.json", case_set="Garrick")
solver_obj.solve()
results = solver_obj.output()

print("Feathering Parameter: " + str(results.feathering_parameter))

print("Pitching Energy: " + str(results.pitching_energy_control) + " J.")
print("Heaving Energy: " + str(results.heaving_energy_control) + " J.")

print("Paper: " + str(results.efficiency))
print("Garrick: " + str(results.model.efficiency_garrick_coefficients))

print("Froude Efficiency: " + str(results.froude_efficiency))
results.show_movement()

results.show_lift_drag()
results.show_power()
results.show_moment_theta(show_plot=True)

# animation.animate(results.time, results.theta, results.h)

