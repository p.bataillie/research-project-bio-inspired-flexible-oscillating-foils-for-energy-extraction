from utils import solver

solver_obj = solver.Solver(filepath="cases/experimental.json", case_set="Theodorsen")
solver_obj.solve()
results = solver_obj.output()

results.show_movement(show_plot=False)
results.show_lift_drag(show_plot=True)
