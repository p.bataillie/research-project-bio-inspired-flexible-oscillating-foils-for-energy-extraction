import numpy as np


def generate_cases(mpi_size):
    cases = []

    pitching_amplitude_vect = np.arange(0.0, 360.0, 10.0)
    heaving_amplitude_vect = np.arange(0.0, 360.0, 10.0)

    for i in range(np.size(pitching_amplitude_vect)):
        for j in range(np.size(heaving_amplitude_vect)):
            cases.append([pitching_amplitude_vect[i], heaving_amplitude_vect[j]])

    cases = np.array_split(np.array(cases), mpi_size)
    return cases
