import sys
import os
from mpi4py import MPI
import numpy as np
import timeit
import sys
import parallel_tools
import opti_tools


comm = MPI.COMM_WORLD
rank = comm.Get_rank()
mpi_size = comm.Get_size()
procname = MPI.Get_processor_name()

optim = opti_tools.Optimisation("cases/cases.json", "opti")

if rank == 0:
    print("MPI Initialization...")

    # if not os.path.exists("log/"):
    #     os.mkdir("log/")
    #
    # for f in os.listdir("log/"):
    #     os.remove("log/" + f)

print("Hello world from processor {}, rank {:d} out of {:d} processors".format(procname, rank, mpi_size))


if rank == 0:
    cases = parallel_tools.generate_cases(mpi_size)
else:
    cases = None

cases = comm.scatter(cases, root=0)

itr = 0

flag = False

x = []
output = []

for case in cases:
    eff = optim.solve(case)

    x.append(case)
    output.append(eff)

    itr = itr + 1

    print("Rank " + str(rank) + " | Cases " + str(itr) + "/" + str(len(cases)) + " | Output = " + str(eff))
    sys.stdout.flush()

x = comm.gather(x, root=0)
output = comm.gather(output, root=0)

if rank == 0:
    x_local = []
    output_local = []

    for i in range(len(x)):
        x_local = x_local + x[i]

    for i in range(len(x)):
        output_local = output_local + output[i]

    output_local = np.array(output_local)

    print("TERMINATED" + "\n")

    print("Best Output: " + str(output_local.max()))
    print('Input: ' + str(x_local[output_local.argmax()]))

MPI.Finalize()
