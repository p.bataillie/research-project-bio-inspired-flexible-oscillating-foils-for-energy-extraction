import copy
import json
import sys
from utils import solver, input_data

sys.path.append('../')


class Optimisation:
    def __init__(self, filepath, case_set):
        # self.experimental_data = input_data.load_data(filepath, case_set, scaling=False)[0]

        self.case_set = case_set
        self.data_file = open(filepath)
        self.json_file = self.data_file.read()
        self.data_dict = json.loads(self.json_file)

    def solve(self, pitching_amplitude=None, frequency_dimensionless=None, pitching_phase=None, heaving_phase=None,
              xp=None, heaving_percentage=None):
        data_dict = self.data_dict[self.case_set][0]

        if pitching_amplitude is not None:
            data_dict['movement']['pitching_amplitude'] = pitching_amplitude

        if frequency_dimensionless is not None:
            data_dict['movement']['frequency'] = (frequency_dimensionless * data_dict['flow']['freestream_velocity']) / (data_dict['airfoil']['chord'] / 2.0)

        if pitching_phase is not None:
            data_dict['movement']['pitching_phase'] = pitching_phase

        if heaving_phase is not None:
            data_dict['movement']['heaving_phase'] = heaving_phase

        if xp is not None:
            data_dict['airfoil']['rotation_axis_in_percentage_chord'] = xp

        if heaving_percentage is not None:
            data_dict['movement']['heaving_amplitude'] = heaving_percentage * data_dict['airfoil']['chord']

        initial_data = input_data.InputData(data_dict)
        tmp = copy.deepcopy(initial_data)
        scaled_data = input_data.theodorsen_garrick_scaling(tmp)

        solver_obj = solver.Solver(initial_data=[initial_data], scaled_data=[scaled_data])
        solver_obj.solve()
        results = solver_obj.output()

        return results
