# Research Project - Bio Inspired Flexible Oscillating Foils for Energy Extraction

**Abstract:** A parametric study is conducted for energy harvesting systems composed of a single oscillating foil operating in steady flow. The most relevant parameters for power production, namely reduced frequency, pitching and heaving amplitudes and the phase shift between pitching and heaving motion have been considered. To compute the power extraction efficiency, models based on potential flow theory have been used: Theodrosen's model provides the lift and the moments while Garrick provides the drag experienced by a 2D flat plate. Additionally, the limitations of those considered models are investigated. This study shows that for achieving high efficiency in a power extraction regime, the assumptions used for the models (e.g. inviscid fluid, low amplitude oscillations) are quite constraining.

## Requirements

Please follow these steps before using the project:

conda create -n oscillating_foil python=3.11 \
conda activate oscillating_foil \
pip install -r "requirements.txt"

For using the Neural Network section of this project, the following commands have to be executed:

pip install torch~=2.5.1 \
conda install -c conda-forge mpi4py openmpi

A Fortran Compiler is required to run the LDVM model. Version used: **GNU Fortran (Ubuntu 7.5.0-3ubuntu1~18.04) 7.5.0**.\
Regarding the execution of the LDVM model, the original Fortran code is executed from Python in a temporary folder.

## Use the Project

To use the project, 3 Python scripts are provided:

**main_theodorsen.py** - To use Theodorsen model according to kinematics declared in a JSON file. \
**main_garrick.py** - To use Garrick model according to kinematics declared in a JSON file. \
**main_ldvm.py** - To use LDVM model according to kinematics declared in a JSON file. \
**main_ldvm_func.py** - To use LDVM model according to kinematics declared in a Python function. \

## Models Implemented

Here are the papers related to the models implemented in this project.

Theodorson T., General Theory of Aerodynamic Instability and the Mechanism of Flutter, NACA Technical Report 496, Langley Memorial Aeronautical Laboratory, 1949. https://ntrs.nasa.gov/citations/19930090935

Garrick, I.E., Propulsion of a flapping and oscillating airfoil, NACA Technical Report 567, Langley Memorial Aeronautical Laboratory, 1936. https://ntrs.nasa.gov/citations/19930091642

Ramesh K, Gopalarathnam A, Granlund K, Ol MV, Edwards JR. Discrete-vortex method with novel shedding criterion for unsteady aerofoil flows with intermittent leading-edge vortex shedding. Journal of Fluid Mechanics. 2014;751:500-538. doi:10.1017/jfm.2014.297

## Validation Data

Theodorsen and Garrick model have been validated with experimental data. This data has been extracted from experiments conducted by Braud A., Ferrand V., Theoretical Model for Energy Harvesting Oscillating Airfoil, ISAE-SUPAERO, 2021 (PDF in the files). The files related to the validation of Theodorsen and Garrick models are located in the **analysis/validation_tg_exp** directory.

LDVM model has been validated with experimental data from: Ōtomo, S., Henne, S., Mulleners, K. et al. Unsteady lift on a high-amplitude pitching aerofoil. Exp Fluids 62, 6 (2021). https://doi.org/10.1007/s00348-020-03095-2. The files related to LDVM validation are located in the **analysis/validation_ldvm** directory.

## Modify the Project

To modify the project, all the class and functions used are located in the **utils** directory. The files related to the parametric analysis are located in the **analysis** directory.
