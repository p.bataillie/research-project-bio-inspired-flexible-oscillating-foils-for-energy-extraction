import sys
sys.path.append('./')
import math
import numpy as np
from utils import solver, results


def test_numerical_example_isaacs():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_greenberg")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')

    greenberg_ref = (1.074
                     - 0.0395 * np.cos(model.w_surge * model.time)
                     + 0.768 * np.sin(model.w_surge * model.time)
                     - 0.074 * np.cos(2.0 * model.w_surge * model.time)
                     - 0.0096 * np.sin(2.0 * model.w_surge * model.time))

    l_0 = - 2.0 * math.pi * model.rho * model.b * (model.u_inf ** 2) * model.theta_m

    output_model = model.lift_force_total.imag / l_0
    output_model_2nd = model.lift_force_test / l_0

    assert np.isclose(results.Results.rmsd(output_model_2nd, greenberg_ref), 0.0, atol=1e-3)
    # assert np.isclose(results.Results.rmsd(output_model, greenberg_ref), 0.0, atol=1e-3)
