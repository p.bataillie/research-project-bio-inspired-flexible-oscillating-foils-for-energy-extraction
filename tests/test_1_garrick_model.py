import math
import sys

import pytest

sys.path.append('./')
from utils import solver, results
import numpy as np


def test_drag_s_complex_real():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_garrick")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')
    assert np.isclose(results.Results.rmsd(model.S.imag, model.S_real), 0.0)


@pytest.mark.skip
def test_check_garrick_coefficients():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_garrick")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')

    assert np.isclose(model.A1, model.a1)
    assert np.isclose(model.A2, (model.a2 + model.b2))
    assert np.isclose(model.A3, (model.a3 + model.c3))
    assert np.isclose(model.A4, (model.a4 + model.b4))
    assert np.isclose(model.A5, (model.a5 + model.c5))
    assert np.isclose(model.A6, (model.a6 + model.b6 + model.c6))


@pytest.mark.skip
def test_mean_drag_s_component():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_garrick")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')

    time_averaged_drag_s_coefficients_a = math.pi * model.rho * model.b * (model.w ** 2) * (model.a1 * (model.h_0 ** 2)
                                                                                                      + model.a2 * (model.theta_0 ** 2)
                                                                                                      + model.a3 * (model.delta_0 ** 2)
                                                                                                      + 2.0 * model.a4 * model.theta_0 * model.h_0
                                                                                                      + 2.0 * model.a5 * model.delta_0 * model.h_0
                                                                                                      + 2.0 * model.a6 * model.theta_0 * model.delta_0)

    time_averaged_drag_s_integral = (model.w / (2.0 * math.pi)) * np.trapz(model.rho * math.pi * (model.S.imag ** 2), x=model.time)
    time_averaged_drag_s_coefficients_m_n = ((math.pi * model.rho) / 4.0) * ((model.M_real ** 2) + (model.N_real ** 2))

    assert np.isclose(time_averaged_drag_s_integral, time_averaged_drag_s_coefficients_m_n)
    assert np.isclose(time_averaged_drag_s_coefficients_a, time_averaged_drag_s_integral)
    assert np.isclose(time_averaged_drag_s_coefficients_a, time_averaged_drag_s_coefficients_m_n)


def test_mean_drag_lift_force_airfoil_component():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_garrick")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')

    time_averaged_drag_alpha_lift_coefficient = math.pi * model.rho * model.b * (model.w ** 2) * (model.b2 * (model.theta_0 ** 2)
                                                                                                  + 2.0 * model.b4 * model.theta_0 * model.h_0
                                                                                                  + 2.0 * model.b6 * model.theta_0 * model.delta_0)

    time_averaged_drag_alpha_lift_integral = (model.w / (2.0 * math.pi)) * np.trapz(model.theta.imag * model.lift_force_airfoil_real, x=model.time)

    assert np.isclose(time_averaged_drag_alpha_lift_coefficient, time_averaged_drag_alpha_lift_integral)


@pytest.mark.skip
def test_mean_drag_lift_force_aileron_component():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_garrick")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')

    time_averaged_drag_beta_lift_coefficient = math.pi * model.rho * model.b * (model.w ** 2) * (model.c3 * (model.delta_0 ** 2)
                                                                                                 + 2.0 * model.c5 * model.delta_0 * model.h_0
                                                                                                 + 2.0 * model.c6 * model.theta_0 * model.delta_0)

    time_averaged_drag_beta_lift_integral = (model.w / (2.0 * math.pi)) * np.trapz(model.delta.imag * model.lift_force_aileron_real, x=model.time)

    assert np.isclose(time_averaged_drag_beta_lift_coefficient, time_averaged_drag_beta_lift_integral)


@pytest.mark.skip
def test_mean_drag():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_garrick")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')

    time_averaged_drag_with_coefficients_1st = model.time_averaged_drag_coefficients
    time_averaged_drag_with_coefficients_2nd = model.time_averaged_drag_coefficients_2nd
    time_averaged_drag_with_px_integral = (model.w / (2.0 * math.pi)) * np.trapz(model.px_real, x=model.time)

    assert np.isclose(time_averaged_drag_with_coefficients_1st, time_averaged_drag_with_px_integral)
    assert np.isclose(time_averaged_drag_with_coefficients_2nd, time_averaged_drag_with_px_integral)
    assert np.isclose(time_averaged_drag_with_coefficients_1st, time_averaged_drag_with_coefficients_2nd)


def test_time_averaged_quantities():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_garrick")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')

    assert np.isclose(model.time_averaged_total_work_coefficients, (model.time_averaged_propulsive_force_coefficients * model.u_inf + model.time_averaged_ke_coefficients))


def test_garrick_drag_sign():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_garrick_drag_sign")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')
    mean_drag_coefficient = np.mean(model.drag_coefficient)
    assert mean_drag_coefficient > 0
