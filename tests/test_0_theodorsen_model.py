import sys
sys.path.append('./')
import scipy
import numpy as np
from models import model_theodorsen
from utils import solver, results


def test_theodorsen_function():
    k = 1 / 20

    c_value_check = model_theodorsen.F(k) + 1j * model_theodorsen.G(k)
    c_value = scipy.special.hankel2(1, k) / (1j * scipy.special.hankel2(0, k) + scipy.special.hankel2(1, k))

    assert (np.isclose(c_value_check.real, c_value.real) and np.isclose(c_value_check.imag, c_value.imag))


def test_check_theodorsen_coefficients():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_theodorsen")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')

    assert np.isclose((model.t12 - model.t11), (2.0 * model.t4))
    assert np.isclose((4.0 * model.t2), (model.t4 * (model.t11 + model.t12)))
    assert np.isclose(model.t15, (model.t4 + model.t10))
    assert np.isclose((model.t16 + model.t17), (- (1.0 / 2.0 - model.a) * model.t4 + (1.0 / 2.0) * model.t11))
    assert np.isclose(model.t20, (model.t10 - 2.0 * np.sqrt(1.0 - (model.e ** 2))))
    assert np.isclose(model.t8, (- (1 / 3) * ((1 - (model.e ** 2)) ** (3 / 2)) - model.e * model.t4))


def test_lift_force_airfoil_complex_real():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_theodorsen")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')
    assert np.isclose(results.Results.rmsd(model.lift_force_airfoil_complex.imag, model.lift_force_airfoil_real), 0.0)


def test_lift_force_aileron_complex_real():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_theodorsen")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')
    assert np.isclose(results.Results.rmsd(model.lift_force_aileron_complex.imag, model.lift_force_aileron_real), 0.0)


def test_moment_theta_complex_real():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_theodorsen")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')
    assert np.isclose(results.Results.rmsd(model.moment_theta_complex.imag, model.moment_theta_real), 0.0)


def test_moment_delta_complex_real():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_theodorsen")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')
    assert np.isclose(results.Results.rmsd(model.moment_delta_complex.imag, model.moment_delta_real), 0.0)


def test_theodorsen_lift_sign():
    solver_obj = solver.Solver(filepath="tests/cases/test_cases.json", case_set="test_case_theodorsen_lift_sign")
    solver_obj.solve()
    model = solver_obj.output(output_type='model')
    mean_lift_coefficient = np.mean(model.lift_coefficient)
    assert mean_lift_coefficient < 0
